<?php

class STPR_View_Helper_WaitingList extends Zend_View_Helper_Abstract
{

    protected $_last_error = false;
    protected $waiting_list = array(
        9924308, 
        9924309,
        9924310,
        9924311,
        9924312,
        9924313,
        9925999,
        9926000,
        9926001,
        9926002,
        9926003,
        9924314,
        9924315,
        9924316,
        9924317,
        9924319,
        9927131,
        9927132,
        9927133,
        9927134,
        9927135,
        9927137,
        9927138,
        9927139,
        9927140,
        9927141,
        9927142,
        9927143,
        9927144,
        9927145,
        9927146,
        9927147,
        1254027,
        9926390,
        9926391,
        9926394,
        9926395,
        9927108,
        9927110,
        9927111,
        9927148,
        9927149,
        9927150,
        9927112,
        9927113,
        9927151,
    );

    public function WaitingList($article_code)
    {
        if (in_array($article_code, $this->waiting_list)) 
		{
			return true;
		}
		else {
			return false;
		}
    }
}