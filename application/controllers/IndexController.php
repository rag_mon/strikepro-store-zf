<?php

class IndexController extends App_Controller_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('site/bootstrap-layout');
    	return true;
    }


}

