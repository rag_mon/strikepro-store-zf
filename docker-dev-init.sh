#!/usr/bin/env bash

LARADOCK_PATH='laradock'

cd $LARADOCK_PATH

echo 'Initializing docker services...'
docker-compose up -d nginx mysql