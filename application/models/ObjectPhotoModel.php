<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 23.08.2016
 * Time: 17:09
 */
require_once 'Zend/Db/Table/Abstract.php';


class Application_Model_ObjectPhoto extends Zend_Db_Table_Abstract
{
    public $_ID_ = 'cat_code';

    public function __construct()
    {
        parent::__construct();
        $this->_name = 'catalog_photo';
    }

    public function getObjectPhoto($Id)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($this->getAdapter()->quoteInto($this->_ID_ . '= ?', $Id));

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addPhoto($data)
    {
        $this->getAdapter()->insert($this->_name, $data);
        return true;
    }
}