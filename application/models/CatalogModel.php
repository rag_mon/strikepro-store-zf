<?php
/**
 * CatalogModel
 * 
 * @author alexbolin
 * @version 
 */
require_once 'Zend/Db/Table/Abstract.php';
class Application_Model_CatalogModel extends Zend_Db_Table_Abstract
{
    /**
     * The default table name 
     */
    protected $_name = 'catalog_cats';
    
    public function getCatalog($catLink) {
        
        $where = $this->getAdapter()->quoteInto('cat_link = ?', $catLink);
        $select = $this->getAdapter()   ->select()
                                        ->from($this->_name)
                                        ->where($where)
                                        ->where('cat_show_on_site = 1');
        
        $stmt = $this->getAdapter()->query($select);
        // Получение данных
        $result = $stmt->fetch();
        
        return $result;
    }
    
    public function getPath($parentCode) {
    	
        $this->_name = 'catalog_cats';
        $where = $this->getAdapter()->quoteInto('cat_code = ?', $parentCode);
        
        $select = $this->getAdapter()   ->select()
                                        ->from($this->_name)
                                        ->where($where);
        
        $stmt = $this->getAdapter()->query($select);
        
        // Получение данных
        $result = $stmt->fetch();
        
        if($result['cat_sub_of'] == 'root') {
            
            $this->array[] = $result;
            
            return array_reverse($this->array);
        }
        else
        {
            $this->array[] = $result;
            return $this->getPath($result['cat_sub_of']);
        }
    }
    
    public function getSubCats($catCode) {
    	
        $this->_name = 'catalog_cats';
        
        $where = $this->getAdapter()->quoteInto('cat_sub_of = ?', $catCode);
        
        $select = $this->getAdapter()   ->select()
                                        ->from($this->_name)
                                        ->where($where)
                                        ->where('cat_show_on_site = 1')
                                        ->order('cat_name ASC');
        
        $stmt = $this->getAdapter()->query($select);
        
        // Получение данных
        $stmt->setFetchMode(Zend_Db::FETCH_OBJ);
        $result = $stmt->fetchAll();
        return $result;
        
    }
    
    
    /**
     * Взять все товары данной категории
     * @param string $catCode
     */
    public function getArticles($catCode) {
    	
        $this->_name = 'catalog_articles';
        
         
        $where = $this->getAdapter()->quoteInto('article_cat = ?', $catCode);
        
        $select = $this->getAdapter()   ->select()
                                        ->from($this->_name)
                                        ->where($where)
                                        ->order('article_image ASC');
        
        $stmt = $this->getAdapter()->query($select);
        
        // Получение данных
        $result = $stmt->fetchAll();
        
        return $result;
    }
    
	/**
     * Взять конкретный товар по артикулу
     * @param string $ArtNo
     */
    public function getArticle($ArtNo) {
    	
        $this->_name = 'catalog_articles';
        $where = $this->getAdapter()->quoteInto('article_art = ?', $ArtNo);
        $select = $this->getAdapter()   ->select()
                                        ->from($this->_name)
                                        ->where($where);
        
        $stmt = $this->getAdapter()->query($select);
        
        // Получение данных
        $result = $stmt->fetch();
        
        return $result;
    }

    public function newItems()
    {

        return $this->newArticles();
    }

    public function newProductGroups()
    {

    }

    public function newArticles()
    {
        $this->_name = 'catalog_articles';
        $where = $this->getAdapter()->quoteInto('article_new = ?', 1);
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($where);

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $result = $stmt->fetchAll();

        return $result;
    }

    
    public function CheckForAvalible($ArtNo) {
    	
    		$this->_name = 'catalog_articles';
    		$where = $this->getAdapter()->quoteInto('article_art = ?', $ArtNo);
    		$select = $this->getAdapter()   ->select()
    		->from( array('p'=> $this->_name), array('art_insale') )
    		->where($where);
    		 
    		$stmt = $this->getAdapter()->query($select);
    		 
    		// Получение данных
    		$result = $stmt->fetchColumn();
    		 
    		return $result;
    }

    public function getProductImages($cat_code)
    {
        $this->_name = 'catalog_images';
        $where = $this->getAdapter()->quoteInto('cat_code = ?', $cat_code);
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($where)
            ->order('primary DESC', 'id ASC');

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $result = $stmt->fetchAll();

        return $result;
    }

    public function getProductPhoto($cat_code)
    {
        $this->_name = 'catalog_photo';
        $where = $this->getAdapter()->quoteInto('cat_code = ?', $cat_code);
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($where)
            ->order('primary DESC', 'id ASC');

        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $result = $stmt->fetchAll();

        return $result;
    }
}
