<?php

require_once 'Zend/Controller/Action.php';
class App_Controller_Action extends Zend_Controller_Action
{
	public function init()
	{
	    if(!$this->checkForMaintanceMode())
	    {
	        $this->_helper->redirector('index', 'maintance', 'default');
        }

        Zend_Session::start();

        /* проверяем, если не используется AJAX, то передаем данные во View */
		if ( !$this->getRequest()->isXmlHttpRequest() )
		{
			/* Инициализация представления */ 
 			$this->initView();
		}

        $this->_helper->_layout->setLayout('site/bootstrap-layout');
	}

	private function checkForMaintanceMode()
    {
        if(file_exists(APPLICATION_PATH . '/configs/maintancemode'))
        {
            if(file_exists(APPLICATION_PATH . '/configs/ip.txt'))
            {
                $clientIP = $this->determineIP();
                $lines = file(APPLICATION_PATH . '/configs/ip.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

                foreach ($lines as $line_num => $line)
                {
                    if($clientIP == $line)
                    {
                        return true;
                    }
                }

                return false;
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }

    private function checkIP($ip)
    {
        if (!empty($ip) && ip2long($ip)!=-1 && ip2long($ip)!=false) {
            $private_ips = array (
                array('0.0.0.0','2.255.255.255'),
                array('10.0.0.0','10.255.255.255'),
                array('127.0.0.0','127.255.255.255'),
                array('169.254.0.0','169.254.255.255'),
                array('172.16.0.0','172.31.255.255'),
                array('192.0.2.0','192.0.2.255'),
                array('192.168.0.0','192.168.255.255'),
                array('255.255.255.0','255.255.255.255')
            );

            foreach ($private_ips as $r) {
                $min = ip2long($r[0]);
                $max = ip2long($r[1]);
                if ((ip2long($ip) >= $min) && (ip2long($ip) <= $max)) return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private function determineIP()
    {
        if (@$this->checkIP($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        foreach (explode(",",@$_SERVER["HTTP_X_FORWARDED_FOR"]) as $ip)
        {
            if (@$this->checkIP(trim($ip)))
            {
                return $ip;
            }
        }
        if (@$this->checkIP($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        } elseif (@$this->checkIP($_SERVER["HTTP_X_CLUSTER_CLIENT_IP"]))  {
            return $_SERVER["HTTP_X_CLUSTER_CLIENT_IP"];
        } elseif (@$this->checkIP($_SERVER["HTTP_FORWARDED_FOR"])) {
            return @$_SERVER["HTTP_FORWARDED_FOR"];
        } elseif (@$this->checkIP($_SERVER["HTTP_FORWARDED"])) {
            return $_SERVER["HTTP_FORWARDED"];
        } else {
            return $_SERVER["REMOTE_ADDR"];
        }
    }
}