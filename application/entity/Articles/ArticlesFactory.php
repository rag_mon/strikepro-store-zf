<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:19
 */

require_once 'entity/Articles/Article.php';
require_once 'models/ArticleModel.php';


class ArticlesFactory
{
    protected $_model;
    protected $articles = array();
    private static $instance;

    private function __construct($Id)
    {
        $this->_model = new Application_Model_Article();

        foreach ($this->_model->getProductArticles($Id) AS $item){

            $this->push(new Article($item));
        }
    }

    private function push(Article $article)
    {
        $this->articles[] = $article;
    }


    public static function getInstance($Id)
    {
        self::$instance = new self($Id);
        return self::$instance;
    }

    public function get()
    {
        return $this->articles;
    }
}