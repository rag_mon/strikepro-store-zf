<?php

class FeedbackController extends App_Controller_Action
{
	protected $_FeedbackModel;
	
    public function init()
    {
    	parent::init();
        /* Initialize action controller here */
    	$this->_FeedbackModel = new Application_Model_FeedbackModel();
    }

    public function indexAction()
    {
    	//view feedback page
    }

    public function shopAction()
    {
        if($this->getRequest()->isXmlHttpRequest() && $this->getRequest()->isPost())
        {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $data = array (
                'department_id' => 1,
                'firstname' => $this->getRequest()->getParam('firstname'),
                'lastname' => $this->getRequest()->getParam('lastname'),
                'email' => $this->getRequest()->getParam('email'),
                'phone' => $this->getRequest()->getParam('phone'),
                'order_num' => $this->getRequest()->getParam('order'),
                'message' => $this->getRequest()->getParam('message')
            );

            $this->_FeedbackModel->exchange_array($data, 1);

            if($this->_FeedbackModel->addFeed())
            {
                $feed = $this->_FeedbackModel->getFeed();
            }

            $this->_helper->json(
                array(  'status' => 'ok',
                        'request_number' => $feed['id'],
                ));

            return true;
        }
    }

    public function partnerAction()
    {
        if($this->getRequest()->isXmlHttpRequest() && $this->getRequest()->isPost())
        {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $data = array (
                'department_id' => 2,
                'firstname' => $this->getRequest()->getParam('firstname'),
                'lastname' => $this->getRequest()->getParam('lastname'),
                'email' => $this->getRequest()->getParam('email'),
                'phone' => $this->getRequest()->getParam('phone'),
                'legalname' => $this->getRequest()->getParam('legalname'),
                'address' => $this->getRequest()->getParam('address'),
                'business_type' => $this->getRequest()->getParam('business_type'),
                'message' => $this->getRequest()->getParam('message')
            );

            $this->_FeedbackModel->exchange_array($data, 2);

            if($this->_FeedbackModel->addFeed())
            {
                $feed = $this->_FeedbackModel->getFeed();
            }

            $this->_helper->json(
                array('status' => 'ok', 'request_number' => $feed['id'],
                ));

            return true;
        }
    }
    
    public function viewAction()
    {
    	//получаем переменные
    	$id = (int)htmlentities(strip_tags(trim($this->_getParam('id'))) , ENT_QUOTES, "UTF-8");
    	$hash = htmlentities(strip_tags(trim($this->_getParam('h'))) , ENT_QUOTES, "UTF-8");
    	
    	if(!isset($id)) {
    		$this->_redirect('https://shop.strikepro.ru/feedback/');
    	}
    	if(!isset($hash)) {
    		$this->_redirect('https://shop.strikepro.ru/feedback/');
    	}
    	
    	$feed = $this->_FeedbackModel->getDbFeed($id);
    	
    	if ($hash !== $feed['hash']){
    		$this->_redirect('https://shop.strikepro.ru/feedback/');
    	}
    	else {
    		$status = htmlentities(strip_tags(trim($this->_getParam('status'))) , ENT_QUOTES, "UTF-8");
    		if (isset($status)) {
    			
    			if($status == 1) {
    				$this->_FeedbackModel->updateFeedStatus($id);
    			}
    		}
    		$this->view->feed = $feed;
    	}
    }
    
    public function selectdepartmentAction()
    {
    	if($this->getRequest()->isXmlHttpRequest() && $this->getRequest()->isPost() && $this->getRequest()->getParam('department_id')) {
    		
    		$this->_helper->layout->disableLayout();
    		$this->_helper->viewRenderer->setNoRender(true);
    		$department_id = (int) $this->getRequest()->getParam('department_id');
    		echo $this->view->partial('/feedback/departmentforms.phtml', array('department_id' => $department_id) );
    	}
    	else {
    		$this->_redirect('https://shop.strikepro.ru/feedback/');
    	}	
    }
    
    private function sendmailtoclient($feed)
    {
    	$department = $this->_FeedbackModel->getDepartment($feed['department_id']);
    	
    	$message = 'Здравствуйте, ' . $feed['firstname'] .' '. $feed['lastname'] . '!' . "\n";
    	$message = $message. 'Спасибо за оставленное обращение на нашем сайте, для удобства мы присвоили вашему обращению номер #' . $department['short_title'] . '-' .$feed['id'] . "\n";
    	$message = $message. 'В кратчайшие сроки мы постараемся ответить на ваше обращение.' . "\n";
    	$message = $message. 'Вы всегда можете посмотреть статус вашего обращения перейдя по ссылке https://shop.strikepro.ru/feedback/view/id/'	. $feed['id'] . '/h/' . $feed['hash'] . "\n";
    	$message = $message. 'Если вы продолжительное время не получаете ответа на ваш вопрос,'. "\n"
    							.'пожалуйста, напишите нам на адрес ' . $department['email'] . ', указав в письме номер (#'.$department['short_title'].'-'.$feed['id'].') вашего обращения.' . "\n";
    	$message = $message. "\n\n";
    	$message = $message. 'C уважением, Команда StrikePro.' . "\n";
    	
    	$config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
    	$tr_conf = array(
                    'auth' => $config->mail->auth,
                    'port' => $config->mail->port,
                    'ssl'   => $config->mail->ssl,
                    'username' => $config->mail->username,
                    'password' => $config->mail->password);
    	$transport = new Zend_Mail_Transport_Smtp($config->mail->smtp, $tr_conf);
    	 
    	$mail = new Zend_Mail('UTF-8');
    	$mail->setBodyText($message);
    	$mail->setFrom($config->mail->username, 'StrikePro.ru');
    	$mail->setReplyTo($department['email'], 'StrikePro.ru');
    	$mail->addTo($feed['email'], $feed['email']);
    	$mail->setSubject('StrikePro.ru - ваше обращение зарегистрировано под номером ' . $department['short_title'] . '-' .$feed['id']);
    	@$mail->send($transport);
    }
    
    private function sendmailtodepartment($feed)
    {
    	$department = $this->_FeedbackModel->getDepartment($feed['department_id']);
    	
    	if ($feed['department_id'] == 1) {
    		
    		$message = 'Номер обращения: ' . $department['short_title'] . $feed['id'] . "\n";
    		$message = 'Имя Фамилия: ' 	. html_entity_decode($feed['firstname'], ENT_QUOTES, "utf-8") 
    									.' '. html_entity_decode($feed['lastname'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Электропочта: ' . html_entity_decode($feed['email'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Номер Заказа: ' . html_entity_decode($feed['order_num'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Телефон: ' . html_entity_decode($feed['phone'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Когда звонить: ' . html_entity_decode($feed['phone_time'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Тема: ' .  html_entity_decode($feed['theme'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Сообщение: ' .  html_entity_decode($feed['message'], ENT_QUOTES, "utf-8") . "\n";
    	
    		$config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
            $tr_conf = array(
                        'auth' => $config->mail->auth,
                        'port' => $config->mail->port,
                        'ssl'   => $config->mail->ssl,
                        'username' => $config->mail->username,
                        'password' => $config->mail->password);
            $transport = new Zend_Mail_Transport_Smtp($config->mail->smtp, $tr_conf);
    		
    		$mail = new Zend_Mail('UTF-8');
    		$mail->setBodyText($message);
    		$mail->setFrom($config->mail->username, 'StrikePro.ru');
    		$mail->addTo($department['email'], $department['email']);
    		$mail->setSubject('StrikePro.RU: # '. $department['short_title'].$feed['id'].' тема : '. html_entity_decode($feed['theme'], ENT_QUOTES, "utf-8"));
    		$mail->setReplyTo(html_entity_decode($feed['email'], ENT_QUOTES, "utf-8"), 
    							html_entity_decode($feed['firstname'], ENT_QUOTES, "utf-8") 
    							.' '. html_entity_decode($feed['lastname'], ENT_QUOTES, "utf-8"));
    		@$mail->send($transport);
    	}
    	
    	if ($feed['department_id'] == 2) {
    	
    		$message = 'Номер обращения: ' . $department['short_title'] . $feed['id'] . "\n";
    		$message = 'Имя Фамилия: ' . $feed['firstname'] .' '. $feed['lastname'] . "\n";
    		$message = $message. 'Электропочта: ' . $feed['email'] . "\n";
    		$message = $message. 'Название Юр. лица: ' . $feed['legalname'] . "\n";
    		$message = $message. 'Адрес: ' . $feed['address'] . "\n";
    		$message = $message. 'Основная деятельность: ' . $feed['business_type'] . "\n";
    		$message = $message. 'Телефон 1: ' . $feed['phone'] .' ('.$feed['phone_type'].') ' . ' - звонить: '. $feed['phone_time'] . "\n";
    		$message = $message. 'Телефон 2: ' . $feed['phone2'] .' ('.$feed['phone2_type'].') ' . ' - звонить: '. $feed['phone2_time'] . "\n";
    		$message = $message. 'Сообщение: ' . html_entity_decode($feed['message'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Публичная ссылка: ' . 'https://shop.strikepro.ru/feedback/view/id/'. $feed['id'] . '/h/' . $feed['hash'] ."\n";
    		$message = $message. 'После отработки заявки обновить статус (решено): '.'https://shop.strikepro.ru/feedback/view/id/'.$feed['id'].'/h/'.$feed['hash'].'/status/1/'."\n";
    		$config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
            $tr_conf = array(
                    'auth' => $config->mail->auth,
                    'port' => $config->mail->port,
                    'ssl'   => $config->mail->ssl,
                    'username' => $config->mail->username,
                    'password' => $config->mail->password);
            $transport = new Zend_Mail_Transport_Smtp($config->mail->smtp, $tr_conf);
    	
    		$mail = new Zend_Mail('UTF-8');
    		$mail->setBodyText($message);
    		$mail->setFrom($config->mail->username, 'StrikePro.ru');
    		$mail->addTo($department['email'], $department['email']);
    		$mail->setSubject('Пришла новая заявка с сайта');
    		@$mail->send($transport);
    	}
    	
    	if($feed['department_id'] == 3 || $feed['department_id'] == 4 || $feed['department_id'] == 5) {
    		
    		$message = 'Номер обращения: ' . $department['short_title'] . $feed['id'] . "\n";
    		$message = 'Имя Фамилия: ' . $feed['firstname'] .' '. $feed['lastname'] . "\n";
    		$message = $message. 'Электропочта: ' . $feed['email'] . "\n";
    		$message = $message. 'Телефон 1: ' . $feed['phone'] .' ('.$feed['phone_type'].') ' . ' - звонить: '. $feed['phone_time'] . "\n";
    		$message = $message. 'Когда звонить: ' . $feed['phone_time'] . "\n";
    		$message = $message. 'Сообщение: ' . html_entity_decode($feed['message'], ENT_QUOTES, "utf-8") . "\n";
    		$message = $message. 'Публичная ссылка: ' . 'https://shop.strikepro.ru/feedback/view/id/'. $feed['id'] . '/h/' . $feed['hash'] ."\n";
    		$message = $message. 'После отработки заявки обновить статус (решено): '.'https://shop.strikepro.ru/feedback/view/id/'.$feed['id'].'/h/'.$feed['hash'].'/status/1/'."\n";

            $config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
            $tr_conf = array(
                    'auth' => $config->mail->auth,
                    'port' => $config->mail->port,
                    'ssl'   => $config->mail->ssl,
                    'username' => $config->mail->username,
                    'password' => $config->mail->password);
            $transport = new Zend_Mail_Transport_Smtp($config->mail->smtp, $tr_conf);
    		
    		$mail = new Zend_Mail('UTF-8');
    		$mail->setBodyText($message);
    		$mail->setFrom($config->mail->username, 'StrikePro.ru');
    		$mail->addTo($department['email'], $department['email']);
    		$mail->setSubject('Пришла новая заявка с сайта');
    		@$mail->send($transport);
    	}
    }
}

