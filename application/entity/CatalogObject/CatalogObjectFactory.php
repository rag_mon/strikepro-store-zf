<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 19.08.2016
 * Time: 14:04
 */

require_once 'entity/CatalogObject/CatalogObject.php';
require_once 'entity/CatalogObject/CatalogObjectTypeDirectory.php';
require_once 'entity/CatalogObject/CatalogObjectProductDirectory.php';
require_once 'models/CatalogObjectModel.php';
require_once 'models/GoodsModel.php';
require_once 'entity/Goods/GoodsFactory.php';


class CatalogObjectFactory extends AbstractCatalogObjectFactory
{
    private $_model;
    private $_modelGoods;

    public function __construct()
    {
        $this->_model = new Application_Model_CatalogObject();
        $this->_modelGoods = new Application_Model_Goods();
    }

    /**
     * @param int $Id
     * @return CatalogObject|CatalogObjectProductDirectory|CatalogObjectTypeDirectory
     * @throws Zend_Http_Exception
     */
    public function getCatalogObject($Id, $type = null)
    {
        // Groups
        if (is_null($type) || $type == 'catalog') {
            if($this->_model->RecordExits($Id)) {
                switch ($this->_model->getCatalogObject($Id)->cat_object_type) {
                    // Directory
                    case 'Рыболовный товар':
                        return CatalogObjectTypeDirectory::getInstance($Id);
                        break;

                    // Product directory
                    default:
                        return CatalogObjectProductDirectory::getInstance($Id);
                }
            }
            else {
                throw new Zend_Http_Exception('Запрашиваемый объект не найден', 404);
            }
        }
        // Articles
        else {
            if($this->_modelGoods->RecordExits($Id)) {
                return GoodsFactory::getInstance($Id)->get();
            }
            else {
                throw new Zend_Http_Exception('Запрашиваемый объект не найден', 404);
            }
        }
    }

    public function getVirtual($type, $param = null)
    {
        return CatalogObjectTypeDirectory::getInstance(0, $type);
    }

    public function getCatalogObjectByLink($Id)
    {
        if($this->_model->RecordExitsByLink($Id)) {

            switch ($this->_model->getCatalogObjectByLink($Id)->cat_object_type) {
                // Directory
                case 'Рыболовный товар':
                    return CatalogObjectTypeDirectory::getInstance($Id, 'link');
                    break;

                // Product directory
                default:
                    return CatalogObjectProductDirectory::getInstance($Id, 'link');
            }
        }
        else {
            throw new Zend_Http_Exception('Запрашиваемый объект не найден', 404);
        }
    }


}