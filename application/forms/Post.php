<?php
class Application_Form_Post extends Zend_Form
{
    public function init()
    {
        parent::init();
        $this->addElementPrefixPath('App_Validate', 'App/Validate/', 'validate');
    }

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->setAction('/post/store');

        $element = new Zend_Form_Element_Text('title');
        $element->setLabel('title')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidators(array(
                array('NotEmpty', true),
            ));

        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('description');

        $element->setLabel('description')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidators(array(
                array('NotEmpty', true),
            ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('link');
        $element->setLabel('link')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidators(array(
                array('NotEmpty', true),
            ));

        $this->addElement($element);

        $element = new Zend_Form_Element_Text('headimage');
        $element->setLabel('headimage')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidators(array(
                array('NotEmpty', true),
            ));

        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('body');

        $element->setLabel('body')
            ->setRequired(true)
            ->addFilter('HtmlEntities')
            ->addFilter('StringTrim')
            ->addValidators(array(
                array('NotEmpty', true),
            ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('posted_at');
        $element->setLabel('posted at')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidators(array(
                array('NotEmpty', true),
                array('DateTime')
            ));

        $this->addElement($element);
    }
}