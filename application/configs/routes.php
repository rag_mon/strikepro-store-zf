<?php

/**
 * Файл формирования маршрутов. Происходит инициализация объекта маршрутизации и задание правил маршрутизации
 */

$front = Zend_Controller_Front::getInstance();
$router = $front->getRouter();

$router->addRoute('blog',
    new Zend_Controller_Router_Route('blog/show/:link', array('controller' => 'blog', 'action' => 'show', 'link' => null))
);

$router->addRoute('post-edit',
    new Zend_Controller_Router_Route('post/edit/:Id', array('controller' => 'post', 'action' => 'edit'))
);
$router->addRoute('post-patch',
    new Zend_Controller_Router_Route('post/patch/:Id', array('controller' => 'post', 'action' => 'patch'))
);

$router->addRoute('file-delete',
    new Zend_Controller_Router_Route('files/delete/:file', array('controller' => 'files', 'action' => 'delete'))
);

$router->addRoute('post-destroy',
    new Zend_Controller_Router_Route('post/destroy', array('controller' => 'post', 'action' => 'destroy'))
);

$router->addRoute('catalog-goods',
    new Zend_Controller_Router_Route('catalog/goods/:catLink', array('controller' => 'catalog', 'action' => 'article', 'catLink' => null))
);

$router->addRoute('new',
    new Zend_Controller_Router_Route('new', array('controller' => 'new', 'action' => 'index'))
);

$router->addRoute('catalog',
     new Zend_Controller_Router_Route('catalog/:catLink', array('controller' => 'catalog', 'action' => 'index', 'catLink' => null))
);

$router->addRoute('product',
    new Zend_Controller_Router_Route('product/:code', array('controller' => 'product', 'action' => 'index', 'code' => null))
);

$router->addRoute('test',
    new Zend_Controller_Router_Route('product/test', array('controller' => 'product', 'action' => 'test'))
);

$router->addRoute('extreme',
    new Zend_Controller_Router_Route('product/extreme', array('controller' => 'product', 'action' => 'extreme'))
);

$router->addRoute('update-features',
    new Zend_Controller_Router_Route('product/update-features', array('controller' => 'product', 'action' => 'update-features'))
);

$router->addRoute('update-description',
    new Zend_Controller_Router_Route('product/update-description', array('controller' => 'product', 'action' => 'update-description'))
);

$router->addRoute('delete-image',
    new Zend_Controller_Router_Route('product/delete-image', array('controller' => 'product', 'action' => 'delete-image'))
);

$router->addRoute('edit-image',
    new Zend_Controller_Router_Route('product/edit-image', array('controller' => 'product', 'action' => 'edit-image'))
);

$router->addRoute('image-info',
    new Zend_Controller_Router_Route('product/image-info', array('controller' => 'product', 'action' => 'image-info'))
);

$router->addRoute('update-image',
    new Zend_Controller_Router_Route('product/update-image', array('controller' => 'product', 'action' => 'update-image'))
);

$router->addRoute('upload-image',
    new Zend_Controller_Router_Route('product/upload-image', array('controller' => 'product', 'action' => 'upload-image'))
);

$router->addRoute('upload-photo',
    new Zend_Controller_Router_Route('product/upload-photo', array('controller' => 'product', 'action' => 'upload-photo'))
);

$router->addRoute('photo-info',
    new Zend_Controller_Router_Route('product/photo-info', array('controller' => 'product', 'action' => 'photo-info'))
);

$router->addRoute('edit-photo',
    new Zend_Controller_Router_Route('product/edit-photo', array('controller' => 'product', 'action' => 'edit-photo'))
);

$router->addRoute('delete-photo',
    new Zend_Controller_Router_Route('product/delete-photo', array('controller' => 'product', 'action' => 'delete-photo'))
);

$router->addRoute('upload-progress',
    new Zend_Controller_Router_Route('product/upload-progress', array('controller' => 'product', 'action' => 'upload-progress'))
);

//WSS 26.07
$router->addRoute('payment_delivery',
     new Zend_Controller_Router_Route('payment_delivery/', array('controller' => 'paymentdelivery', 'action' => 'index', 'catLink' => null))
);
//WSS
$router->addRoute('order_by_num',
     new Zend_Controller_Router_Route('order/:ordernum', array('controller' => 'cart', 'action' => 'displayorder', 'ordernum' => null))
);
$router->addRoute('order_by_numB',
     new Zend_Controller_Router_Route('o/:ordernum', array('controller' => 'cart', 'action' => 'displayorder', 'ordernum' => null))
);
$router->addRoute('order_by_numC',
     new Zend_Controller_Router_Route('zakaz/:ordernum', array('controller' => 'cart', 'action' => 'displayorder', 'ordernum' => null))
);
$router->addRoute('order_by_numD',
     new Zend_Controller_Router_Route('z/:ordernum', array('controller' => 'cart', 'action' => 'displayorder', 'ordernum' => null))
);
$router->addRoute('order_by_numD',
     new Zend_Controller_Router_Route('av/:ordernum', array('controller' => 'cart', 'action' => 'av', 'ordernum' => null))
);
$router->addRoute('payment_ok',
     new Zend_Controller_Router_Route('order/paymentok', array('controller' => 'cart', 'action' => 'paymentok'))
);
$router->addRoute('paymenterror',
     new Zend_Controller_Router_Route('order/paymenterror', array('controller' => 'cart', 'action' => 'paymenterror'))
);

$router->addRoute('getajaxproducts',
    new Zend_Controller_Router_Route('cart/getajaxproducts', array('controller' => 'cart', 'action' => 'getajaxproducts'))
);
