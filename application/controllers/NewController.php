<?php

require_once 'App/Controller/Action.php';
require_once 'entity/CatalogObject/AbstractCatalogObjectFactory.php';

class NewController extends App_Controller_Action {

    public function indexAction()
    {
        if (!is_object($this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getVirtual('new'))) {
            throw new Zend_Http_Exception('Запрашиваемый ресурс не найден', 404);
        }
        else {
            $this->catalog_object->withChilds();
        }
        $this->view->catalog_object = $this->catalog_object;

        $this->_helper->viewRenderer($this->catalog_object->getPattern());
    }

}
