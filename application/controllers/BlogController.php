<?php

require_once 'App/Controller/Action.php';
require_once 'entity/Post/Post.php';

class BlogController extends App_Controller_Action
{
    protected $post;

    public function init()
    {
        parent::init();
        $this->post = new Application_Entity_Post_Post();
    }

    public function indexAction()
    {
        $this->view->posts = $this->post->posted()->last()->all();
    }

    public function showAction()
    {
        $this->view->post = $this->post->where(['link', strval($this->_request->getParam('link'))])->first();
    }
}