$('#postsBox').on('click', '.destroyPost', function(event) {
    event.preventDefault();
    $('#confirmDestroyPostButton').attr('data-id', $(this).attr('data-id'));
    $('#destroyPostModal').modal('show');
});

$('#confirmDestroyPostButton').on('click', function(event) {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "<?php echo $this->JsEscape($this->baseUrl('post/destroy')); ?>",
        data: { Id: $(this).attr('data-id'), format: "json" }
    })
        .done(function(data) {
            if(data.msg == 1) {

                $('#post_' + data.Id).fadeOut().remove();
                $.jGrowl("Запись #"+ data.Id  +" успешно удалена." , { life: 10000 });
            }
            else {
                alert('какая то ошибка! +' + data.msg);
            }
        })
        .fail(function() {
            alert('какая то ошибка (fail)!');
        });

    $('#destroyPostModal').modal('hide');
});