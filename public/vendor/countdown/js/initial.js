/**
 * Created by Aleksandr Taran on 30.08.2016.
 */

$(document).ready( function() {
    $('#clock').countdown('2016/09/10', function(event) {
        $(this).find('#daysLeft').html(event.strftime('%-D'));
        $(this).find('#hours').html(event.strftime('%-H'));
        $(this).find('#minutes').html(event.strftime('%M'));
        $(this).find('#seconds').html(event.strftime('%S'));
    });
});


