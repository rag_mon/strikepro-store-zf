<?php

require_once 'entity/FileManager/File.php';

class Application_Entity_FileManager_FileManager
{
    private $_ROOT;
    private $_URL = 'upload';
    private $files;

    public function __construct()
    {
		$this->_ROOT = APPLICATION_PATH . '/../public/upload';
        $this->scan();
    }


    public function scan()
    {
        $files = array_diff(scandir($this->_ROOT), array('.', '..'));

        foreach($files AS $file) {

            $this->files[] = new Application_Entity_FileManager_File($file);
        }
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getURL()
    {
        return $this->_URL;
    }

}