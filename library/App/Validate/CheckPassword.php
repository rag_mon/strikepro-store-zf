<?php

/**
 * App_Validate_NoDbRecordExists
 * 
 * Проверка отсутствия записи в таблице
 * 
 * @author Alexander Bolin
 * 
 */
class App_Validate_CheckPassword extends Zend_Validate_Abstract 
{

    /**
     * Метка ошибки
     * @var const 
     */    
    const INCORRECT = 'Incorrrect Password';
    
    /**
     * Текст ошибки
     * @var array 
     */
    protected $_messageTemplates = array(
        self::INCORRECT => 'Incorrect Password'
    );

    /**
     * Имя таблица в которой будет происходить поиск записи
     * @var string
     */    
    protected $_table = null;

    /**
     * Имя поля по которому будем отбирать строку
     * @var string
     */
     protected $_Identfield = null;
     
    /**
     * Значение поля по которому будем отбирать строку
     * @var string
     */
     protected $_IdentValue = null;
    
    /**
     * Имя поля по которому будет происходить поиск значения 
     * @var string
     */    
    protected $_field = null;    

    /**
     * Используемый адаптер базы данных
     *
     * @var unknown_type
     */    
    protected $_adapter = null;    
       
    /**
     * Конструктор
     * 
     * @param string $table Имя таблицы
     * @param string $field Имя поля
     * @param Zend_Db_Adapter_Abstract $adapter Адаптер базы данных
     */
    public function __construct($table, $Identfield, $field, Zend_Db_Adapter_Abstract $adapter = null)
    {
        $this->_table = $table;
        $this->_field = $field;
        $this->_Identfield = $Identfield;
        
        $session = new Zend_Session_Namespace('Zend_Auth');
        $this->_IdentValue = $session->storage->$Identfield;
        
        if ($adapter == null) {
        	// Если адаптер не задан, пробуем подключить адаптер заданный по умолчанию для Zend_Db_Table
        	$adapter = Zend_Db_Table::getDefaultAdapter();
        	
        	// Если адаптер по умолчанию не задан выбрасываем исключение
        	if ($adapter == null) {
        	   
        		throw new Exception('No user and no default adapter was found');
        	}
        }
        
        $this->_adapter = $adapter;
    }
    
    /**
     * Проверка
     * 
     * @param string $value значение которое поддается валидации
     */
    public function isValid($value)
    {
        $this->_setValue(sha1($value));

        $adapter = $this->_adapter;
        
        $select = $adapter->select($this->_field)
            ->from( $this->_table, array($this->_field) )
            ->where($this->_Identfield . '= ?', $this->_IdentValue)
            ->limit(1)
            ;
        $stmt = $adapter->query($select);
        $result = $stmt->fetch(Zend_Db::FETCH_COLUMN);
        
        if ($this-> _value == $result) return true;
		
        $this->_error(self::INCORRECT);
        return false;

    }

}

