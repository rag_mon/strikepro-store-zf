<?php
class App_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
 	/**
 	 * Переменные для хранения сущностей Аутентификации и Управления правами
 	 */
 	private $_auth;
 	private $_acl;
 
 	/**
 	 * Определение переходов при недопустимости текущей роли и/или аутентификации
 	 * естественно, такие экшены у вас должны быть созданы и работать :)
 	 */
 	protected $_noAuth = array(
 		'module'     => 'default',
 		'controller' => 'auth',
 		'action'     => 'login');
 	
 	protected $_noAcl  = array(
 		'module'     => 'default',
 		'controller' => 'error',
 		'action'     => 'denied');
 
 	/**
 	 * @param resource (Zend_Auth) Объект аутентификации 
 	 * @param resource (App_Acl) Объект управления правами
 	 * @return void
 	 */
 	public function __construct($auth, $acl)
 	{
  		$this->_auth = $auth;
  		$this->_acl  = $acl;
 	}
 
 	/**
 	 * Перехват функции preDispatch( )
 	 */
 	public function preDispatch( Zend_Controller_Request_Abstract $request)
 	{
	 	if ($this->_response->isException()){
	       $this->getResponse()->setHttpResponseCode(404);
	        return;
	    }
   		
	    /* Определяем параметры запрос */
   		$controller	= $request->controller;
		$action		= $request->action;
		$module     = $request->module;
		$resource   = $request->controller;
   		
   		/**
 	 	 * Если пользователь авторизирован, то получаем его "роль"
 	 	 * (хранится в БД вместе с другой инфой и переноситься в сессию при аутентификации (см. выше)
 	 	 * если нет, то он "гость"
 	 	 */
 	 	if( $this->_auth->hasIdentity() )
 	 	{
   			$id = $this->_auth->getIdentity()->id;
 	 		$admin = new Application_Model_Admins();
 	 		$data = $admin->getAdminById($id);
 	 		$role = $data['group_name'];
 	 		
	 	 	/* Создаем роли */
	 	 	$this->_acl->addRole($role);
	 	 	
	 	 	$modules_model = new Application_Model_Modules();
			
			/* выборка контроллера */
			$controller_data = $modules_model->getContollerByName($controller);
			
			/* выборка экшена */
			$action_data = $modules_model-> getAction($controller_data['controller_id'], $action);
			
			/* выборка прав */
			$access_data = $modules_model->getAccess($action_data['action_id'], $data['group_id']);
			
			/* Добавляем новые ресурсы */
			$this->_acl->add(new Zend_Acl_Resource($controller_data['controller_name']));
			
			/* Правим права */
			if($access_data['accesseble']  == 1 && $action_data['enabled'] == 1) {
				 
				$this->_acl->allow($data['group_name'], $controller_data['controller_name'], $action_data['action_name'] );
			}
			else {
				
				$this->_acl->deny($data['group_name'], $controller_data['controller_name'], $action_data['action_name'] );
			}
			
 	 	}
 	 	else
   		{
   			$role = 'guest';
   			
   			$this->_acl->add(new Zend_Acl_Resource($controller));
   			
   			if($controller == 'auth' && $action == 'login') {
   				
   				$this->_acl->allow($role, $controller, $action);
   			}
   			else {
   				
   				$this->_acl->deny($role, $controller, $action);
   			}
   			
   		}
 	 	
 	 	/* Проверяем уровень доступа пользователя к ресурсу */
   		#if( $this->_acl->has($resource) )
   		#	$resource = null;
   		
   		
 
 	 	/* Если доступ не разрешен - выясняем по какой причине (туда и отсылаем :) */
 		if( !$this->_acl->isAllowed($role, $controller, $action) )
		list($module, $controller, $action)  =( !$this->_auth->hasIdentity() ) ?  array_values($this->_noAuth) : array_values($this->_noAcl);
		
		
		/* Определяем новые данные запроса */
		$request->setModuleName($module);
		$request->setControllerName($controller);
		$request->setActionName($action);
		
 	}
}