<?php

require_once 'App/Controller/Action.php';
class App_Controller_Admin extends App_Controller_Action
{

    public function init()
    {
        parent::init();
        Zend_Session::start();

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->redirector('index', 'index');
        }

        $this->view->admin_messages = $this->_helper->flashMessenger->getMessages();

        // отключаем общий шаблон, включаем бутстрап шаблон
        $this->_helper->_layout->setLayout('admin/bootstrap-layout');
    }

}