/**
 * Created by Aleksandr Taran on 30.08.2016.
 */
$(document).ready(function()
{
    $("#current_product_image").attr("src", function(){
        return $(".product_image_primary").attr("src");
    });

    $(".product_image").click(function () {
        $("#current_product_image").unreel();
        $("#current_product_image").attr("src", $(this).attr("src"));
    });

    $("#product_image_rotation").click(function () {

        $('#current_product_image').reel({
            images: '/products/EG-193B-SP/rotation/images/##.png',
            frames: 24,
            loops: true,
            speed: 0.40,
            revolution: 500,
            clickfree: true,
            tempo: 30,
            delay: 2,
            autoplay: 1,
        });
    });

    $(function() {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
            $('html,body').animate({scrollTop: $('#tab_list').offset().top - 160});
        }
    });

    $("#only_insale").click(function () {
        if($("#only_insale").is(':checked')) {
            $("img[data-insale='0']").parent().fadeTo( "slow", 0.1 );
        }
        else {
            $("img[data-insale='0']").parent().fadeTo( "slow", 1.0 );
        }
    });
});