<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:19
 */

require_once 'models/ArticleModel.php';

class Goods
{
    protected $_model;

    private $property = array(
        'article_art',
        'article_cat',
        'article_new',
        'art_insale',
        'art_price',
        'article_name',
        'article_code',
        'article_color',
        'article_image',
        'article_image_big',
        'article_link',
        'article_description',
        'article_type'
    );

    public function __construct($item)
    {
        if (!empty($item)) {
            foreach ($item as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }
        //var_dump(get_object_vars($this), $this->article_type);exit;

        $config = new Zend_Config_Ini('configs/application.ini', 'production');

//        $this->article_image = $config->products->directory->thumbnail
//            . '/'
//            . $this->article_color
//            . $config->products->directory->thumbnail_ext;
//
//        $this->article_image_big = $config->products->directory->colors
//            . '/'
//            . $this->article_color
//            . $config->products->directory->colors_ext;
    }

//    public function __debugInfo()
//    {
//        //
//    }

    public function getPattern()
    {
        return 'articles';
    }
}