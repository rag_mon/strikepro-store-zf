<?php

class STPR_View_Helper_SaleMark extends Zend_View_Helper_Abstract
{

    protected $_last_error = false;

    public function SaleMark($percent = null)
    {
        if(!$percent) {
            $html = "<div class=\"saleIcon\" style=\" position: absolute; width:32px; height:32px; background: #ffffff url('/images/sale30.png') no-repeat center center;\"></div>";
        }
        if($percent == 15)
        {
            $html = "<div class=\"saleIcon\" style=\" position: absolute; width:32px; height:32px; background: #ffffff url('/images/sale15.png') no-repeat center center;\"></div>";
        }
        return $html;
    }
}