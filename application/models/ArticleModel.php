<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:30
 */

require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_Article extends Zend_Db_Table_Abstract
{
    public $_parent;
    public $_ID_;

    public function __construct()
    {
        parent::__construct();
        $this->_parent = 'article_cat';
        $this->_ID_ = 'article_art';
        $this->_name = 'catalog_articles';
    }

    public function getProductArticles($Id)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($this->getAdapter()->quoteInto($this->_parent . '=?', $Id));

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetchAll();

        return $result;
    }
}