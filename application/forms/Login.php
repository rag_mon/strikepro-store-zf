<?php
class Application_Form_Login extends Zend_Form
{
	public function __construct($options = null)
    {
        parent::__construct($options);
        $this->setName('email');
        
        /* Указываем action формы */
        $this->setAction('/auth/login');
        $this->setAttrib('class', 'profile');

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('email')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $email->setAttrib('class', 'input_text');

        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('Пароль')
				 ->setRequired(true)
            	 ->addFilter('StripTags')
            	 ->addFilter('StringTrim')
            	 ->addValidator('NotEmpty');
        $password->setAttrib('class', 'input_text');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton')
        		->setLabel('войти');
        
		$config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
		
        $captcha = new Zend_Form_Element_Captcha('captcha',
         			array(	'label' => 'Антиспам',  
         					'captcha' => array(
					         'captcha' => 'Image',  
					         'wordLen' => 4,  
					         'timeout' => 300,  
					         'font' => APPLICATION_PATH . '/configs/Pragmatica_Regular.Ttf',  
					         'imgDir' => $config->basepath .'/captcha/',   
					         'imgUrl' => $config->baseUrl . '/captcha/',
 						)));
 		$captcha->setAttrib('class', 'input_text');
 						
 		$this->addElements(array($id, $email, $password, $captcha, $submit));
 		
 		//Разбиваем элементы по группам
		$this->addDisplayGroup(	array('email', 'password', 'captcha', 'submit'),
 								'MarketDataGroup', 
 								array('legend' => '')
							);
    }
}