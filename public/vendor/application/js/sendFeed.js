/**
 * Created by Aleksandr Taran on 05.09.2016.
 */
jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
}, "<br />Пожалуйста, введите номер телефона в формате +71234567890");

$().ready(function()
{
    $("#shop_form").validate({
        rules: {
            firstname: {
                required: true,
                minlength: 1,
            },
            lastname: {
                required: true,
                minlength: 1,
            },
            phone: {
                required: true,
                minlength: 7,
                phoneno:true
            },
            email: {
                required: true,
                email: true,
            },
            order: {
                required: false,
                number: true,
                minlength: 4,
            },
            message: {
                required: true,
                minlength: 10,
            },
        },
        messages: {
            firstname: {
                required: "Пожалуйста введите ваше имя.",
                minlength: "Минимально один символ."
            },
            lastname: {
                required: "Пожалуйста введите вашу фамилию.",
                minlength: "Минимально один символ."
            },
            phone: {
                required: "Пожалуйста введите номер телефона.",
                minlength: "Минимально семь цифр.",
            },
            email: {
                required: "Пожалуйста, введите ваш адрес электронной почты.",
                email: "Пожалуйста, введите правильный адрес электронной почты в формате example@example.com",
            },
            order : {
                number: "Допускается ввод только цифр.",
                minlength: "Минимально четыре символа для номера заказа."
            },
            message: {
                required: "Пожалуйста, опишите цель вашего обращения.",
                minlength: "Минимально 10 символов."
            }
        }
    });
});

function sendFeed()
{
    var form = $("#shop_form");

    if(form.valid()) {
        $.ajax({
            type: 'POST',
            url: ($('head').data('site_url')) + '/feedback/shop',
            data: {
                'firstname': $('#firstname').val(),
                'lastname': $('#lastname').val(),
                'phone': $('#phone').val(),
                'email': $('#email').val(),
                'order': $('#order').val(),
                'message': $('#message').val(),
            },
            success: function (response) {
                if (response.status == 'ok') {
                    $('#request_number').text(response.request_number);
                    $('#shop_form').fadeOut(100);
                    $('#success_add_request').fadeIn(1000);
                }
                return;
            },
            fail: function(){
                alert("Ошибка! Повторите попытку позже.")
            }
        });
    }
    else {
        return;
    }
}