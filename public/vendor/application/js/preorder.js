$(document).ready(function()
{
    $('#PrivacyAgreement').val($(this).is(':checked'));
    $("#makeorderbutton").prop("disabled", true);


    $("#PrivacyAgreement").change(function() {
        if ($(this).is(":checked")) {
            $("#makeorderbutton").prop("disabled", false);
        } else {
            $("#makeorderbutton").prop("disabled", true);
        }
    });
});