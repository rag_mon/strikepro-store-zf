<?php

function myCmp($a, $b)
{
    if(isset($a->cat_name) && isset($b->cat_name)) {
        if(strcasecmp($a->cat_name, $b->cat_name) == 0) return 0;
        return strcasecmp($a->cat_name, $b->cat_name) > 0 ? 1 : -1;
    }

    if(isset($a->article_name) && isset($b->article_name)) {
        if(strcasecmp($a->article_name, $b->article_name) == 0) return 0;
        return strcasecmp($a->article_name, $b->article_name) > 0 ? 1 : -1;
    }

    if(isset($a->cat_name) && isset($b->article_name)) {
        if(strcasecmp($a->cat_name, $b->article_name) == 0) return 0;
        return strcasecmp($a->cat_name, $b->article_name) > 0 ? 1 : -1;
    }

    if(isset($a->article_name) && isset($b->cat_name)) {
        if(strcasecmp($a->article_name, $b->cat_name) == 0) return 0;
        return strcasecmp($a->article_name, $b->cat_name) > 0 ? 1 : -1;
    }
}

require_once 'entity/CatalogObject/CatalogObjectInterface.php';
require_once 'models/CatalogObjectModel.php';
require_once 'models/GoodsModel.php';

class CatalogObject implements CatalogObjectInterface
{
    protected $_model;

    public $child_objects;
    protected $pattern;

    private $property = array(
        'cat_id',
        'cat_code',
        'cat_object_type',
    );

    /**
     * @var self
     */
    private static $instance;


    /**
     * Возвращает экземпляр себя
     *
     * @return self
     */
    public static function getInstance($Id, $id_type = null)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self($Id, $id_type);
        }
        return self::$instance;
    }

    /**
     * Конструктор закрыт
     */
    private function __construct($Id, $id_type = null)
    {
        $this->_model = new Application_Model_CatalogObject();

        if ($id_type == 'link') {
        $DbObject = $this->getCatalogObjectByLink($Id);
        }
        else {
            $DbObject = $this->getCatalogObject($Id);
        }

        if (!empty($DbObject)) {
            foreach ($DbObject as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }

        $this->setPattern('directory');
    }

    public function withChilds()
    {
        if($this->cat_object_type == 'virtual')
        {
            $groups = $this->_model->newProductGroups();
            $articles = $this->_model->newArticles();

            $this->child_objects = $this->sortobjects(array_merge($groups, $articles));
            return $this;
        }
        else {
            $groups = $this->_model->getChildCats($this->cat_code);
            $articles = (new Application_Model_Goods())->getChildArticles($this->cat_code);
            $this->child_objects = $this->sortobjects(array_merge($groups, $articles));
            return $this;
        }

    }

    protected function sortobjects($array)
    {
        $new_items = array();
        $items = array();

        foreach ($array AS $element)
        {
            if(isset($element->cat_name))
            {
                if($element->cat_new) {
                    $new_items[] = $element;
                }
                else {
                    $items[] = $element;
                }
            }

            if(isset($element->article_name))
            {
                if($element->article_new) {
                    $new_items[] = $element;
                }
                else {
                    $items[] = $element;
                }
            }
        }

        uasort($new_items, 'myCmp');
        uasort($items, 'myCmp');

        return array_merge($new_items, $items);
    }


    /**
     * Клонирование запрещено
     */
    private function __clone()
    {

    }

    /**
     * Сериализация запрещена
     */
    private function __sleep()
    {

    }

    /**
     * Десериализация запрещена
     */
    private function __wakeup()
    {

    }

    protected function getCatalogObject($Id)
    {
        return $this->_model->getCatalogObject($Id);
    }

    protected function getCatalogObjectByLInk($Id)
    {
        return $this->_model->getCatalogObjectByLink($Id);
    }

    protected function setPattern($pattern) {

        $this->pattern = $pattern;
    }

    public function getPattern()
    {
        return $this->pattern;
    }



    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }

        $array['child_objects'] = $this->child_objects;

        return $array;
    }
}