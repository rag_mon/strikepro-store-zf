<?php
$config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
require_once 'Zend/Controller/Action.php';
class CartController extends Zend_Controller_Action
{
    protected $_cartModel = false;
	
	public function init()
    {
    	parent::init();
    	$this->_cartModel = new Application_Model_CartModel();
    	return true;
    }
	
	public function indexAction()
	{    
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->json(
                array(
                    'status' => $product_updated? 'ok':'fail',
                    'quantity' => $product_updated,
                    'products' => $this->_cartModel->getCartProducts(),
                )
            );
        }
        else{
            $this->_helper->_layout->setLayout('site/bootstrap-layout');
            $this->view->dlvr_mode_idx = strval($this->_request->getParam('dlvr_mode_idx'));
            $this->view->cart = $this->_cartModel->getCartProducts();
        }
        
        return true;
    }//-indexAction

    public function getajaxproductsAction()
    {
        if($this->getRequest()->isXmlHttpRequest())
        {
            if(!$this->_cartModel)
            {
                $this->_cartModel = new Application_Model_CartModel();
            }

            $cart_products = $this->_cartModel->getCartProducts();

            if (count($cart_products > 0)) {

                $cart_total = 0;
                $cart_count = 0;

                foreach ($cart_products as $product)
                {
                    $cart_count += $product['product_count'];
                    $cart_total += round($product['product_count'] * round($product['art_price'], 2), 2);

                    $products[] = array (
                        'goods_art'  =>  $product['product_art'],
                        'goods_name'    => $product['article_name'] . ' ' . '(' . $product['article_code'] . ')',
                        'goods_price'    => $product['art_price'],
                        'number_of_goods'    => $product['product_count'],
                        'total_cost_goods'    => $product['row_total'],
                    );
                }

                $data = array(
                    'number_of_goods_in_cart' => $cart_count,
                    'cost_of_goods' => $cart_total,
                    'goods_list' => $products
                );
            }
            else {
                $data = array(
                    'number_of_goods_in_cart' => 0,
                    'cost_of_goods' => 0,
                );
            }

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $response = $this->getResponse();
            $response->setHeader('Content-type', 'application/json', true);
            return $this->_helper->json->sendJson($data);
        }
        else {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            exit;
        }
    }

    public function addproductAction()
    {

        $product_art = strval($this->_request->getParam('art'));
        $product_quantity = intval($this->_request->getParam('qty'));
        $product_updated = 0;

        if($product_quantity == 0)
        {
            $product_quantity = 1;
        }

        $this->_cartModel->_current_product = false;

        if($product_art && $product_updated = $this->_cartModel->addToCart($product_art, $product_quantity))
        {
        	//product updated
        }

        if($product_updated && is_array($this->_cartModel->_current_product))
        {
        	$this_product = $this->_cartModel->_current_product;
        }
        else {
        	$this_product = $this->_cartModel->getProduct($product_art);
        }

        if($product_updated && is_array($this_product))
        {
        	$this_product['fullname_fmt'] = ($this_product['cat_codename']) ? $this_product['cat_codename']:'';

            if($this_product['cat_name'] && $this_product['cat_codename'] != $this_product['cat_name'])
            {
				$this_product['fullname_fmt'] .= ' '.$this_product['cat_name'];
            }
        }

        if($this->getRequest()->isXmlHttpRequest())
        {
            require_once APPLICATION_PATH . '/views/helpers/DisplayProducts.php';
            require_once APPLICATION_PATH . '/views/helpers/CountItem.php';

            $cartgen = new STPR_View_Helper_DisplayProducts();
            $CountItem = new STPR_View_Helper_CountItem();
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->_helper->json(
                array(
                    'art' => $product_art,
                    'status' => $product_updated ? 'ok':'fail',
                    'quantity' => $product_updated,
                    'product' => $this_product,
                    'cartblock' => $cartgen->DisplayProducts('popup'),
                    'countitem' => $CountItem->CountItem()
                )
            );
        }
        else {
            if($product_updated)
            {
            	$this->view->message = 'Товар добавлен в корзину: <a href="/catalog/'.($this_product['cat_link']).'">'.(htmlspecialchars($this_product['fullname_fmt'])).'</a>';
            }
            else {
            	$this->view->message = 'Ошибка добавления товара в корзину: товар снят с продажи или не найден по артикулу';
            }
            $this->view->dlvr_mode_idx = strval($this->_request->getParam('dlvr_mode_idx'));
            $this->view->cart = $this->_cartModel->getCartProducts();
            $this->_helper->viewRenderer('index');
        }
        
        return true;
    }//-addproductAction
	
	public function rmproductAction()
	{
        $product_art = strval($this->_request->getParam('art'));
        $this_product = $this->_cartModel->getProduct($product_art);
        if( !$product_art ) {
			$this->_helper->viewRenderer('error');
            $this->view->message = 'Артикул товара не задан';
            return false;
        }
        elseif( !is_array($this_product) ) {
        	$this->_helper->viewRenderer('error');
            $this->view->message = 'Товар не найден по артикулу';
            return false;
        }
        $setQtyState = false;
        $this->_cartModel->_current_product = false;
        if( $setQtyState = $this->_cartModel->setQuantityExact( Array( $product_art => 0) ) ) {
        	//product removed
        }
        $this_product['fullname_fmt'] = ($this_product['cat_codename'])? $this_product['cat_codename']:'';
        if($this_product['cat_name'] && $this_product['cat_codename'] != $this_product['cat_name']){
        	$this_product['fullname_fmt'] .= ' '.$this_product['cat_name'];
        }
        if($this->getRequest()->isXmlHttpRequest()){
        	require_once APPLICATION_PATH . '/views/helpers/DisplayCart.php';
            $cartgen = new STPR_View_Helper_DisplayCart;
            $cartgen = $cartgen->DisplayCart($mode='raw');
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->json(
                array(
                    'status' => $setQtyState? 'ok':'fail',
                    'productIsDeleted' => $setQtyState,
                    'product' => $this_product,
                    'rawcart' => $cartgen,
                )
            );
        }
        else{
        	if($setQtyState && is_array($this_product)){
                
            	$this->view->message = 'Товар удален из корзины: <a href="/catalog/'.($this_product['cat_link']).'">'.(htmlspecialchars($this_product['fullname_fmt'])).'</a>';
            }
            else{
                
            	$this->view->message = 'Ошибка удаления корзину: товар  не найден по артикулу';
            }
            $this->view->dlvr_mode_idx = strval($this->_request->getParam('dlvr_mode_idx'));
            $this->view->cart = $this->_cartModel->getCartProducts();
            $this->_helper->viewRenderer('index');
        }
        return true;
    }//-rmproductAction
	
	public function setquantityAction()
	{
        $submode = strval($this->_request->getParam('submode'));

        if($submode && $submode == 'preorder')
        {
            $dlvr_mode_idx = intval($this->_request->getParam('dlvr_mode_idx'));
            $rnd = rand(10000, 99999);
            $this->_redirect('/cart/preorder?rnd='.$rnd.'&dlvr_mode_idx='.$dlvr_mode_idx);
            return true;
        }

        $arts = $this->_get_suffixed_params('art_row_');
        $qtys = $this->_get_suffixed_params('qty_row_');

        $incl_cart_stats = intval($this->_request->getParam('incl_cart_stats'));
        $exact_qtys = array();

        foreach ( array_keys($arts) as $art_row )
        {
        	$exact_qtys[$arts[$art_row]] = array_key_exists($art_row, $qtys)? intval($qtys[$art_row]):1;
        }

        $setQtyState = false;

        $this->_cartModel->_current_product = false;

        if($setQtyState = $this->_cartModel->setQuantityExact($exact_qtys, $allow_delete=false))
        {
            //product removed
        }

		if( $this->getRequest()->isXmlHttpRequest() )
		{
        	$cartgen = array();
            if( $incl_cart_stats ) {
                require_once APPLICATION_PATH . '/views/helpers/DisplayCart.php';
                $cartgen = new STPR_View_Helper_DisplayCart;
                $cartgen = $cartgen->DisplayCart($mode = 'raw');
            }
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->json(
                array(
                    'status' => $setQtyState? 'ok':'fail',
                    'products' => $this->_cartModel->getCartProducts(),
                    'rawcart' => $cartgen,
                )
            );
        }
        else {
            if( $setQtyState ) {
                $this->view->message = 'Товарам заданы количества';
            }
            else {
                $this->view->message = 'Ошибка уставновления количества товаров в корзине';
            }
            $this->view->cart 			= $this->_cartModel->getCartProducts();
            $this->view->dlvr_mode_idx 	= strval($this->_request->getParam('dlvr_mode_idx'));
            $this->_helper->viewRenderer('index');
        }
        return true;
    }//-setquantityAction

    public function confirmemptycartAction()
    {
        //just confirm button
        return true;
    }//-confirmemptycartAction
	
	public function emptycartAction()
	{
        $emptyCartState = false;

        $this->_cartModel->_current_product = false;

        if( $emptyCartState = $this->_cartModel->emptyCart() ){
            //product removed
        }

        if( $this->getRequest()->isXmlHttpRequest() ) {
            
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->json(
                array(
                    'status' => $emptyCartState? 'ok':'fail',
                    'cartIsClean' => $emptyCartState,
                    'products' => $this->_cartModel->getCartProducts(),
                )
            );
        }
        else {
            if($emptyCartState) {
                $this->view->message = 'Товары удалены из корзины';
            }
            else{
                $this->view->message = 'Ошибка удаления товаров из корзины';
            }
            $this->view->dlvr_mode_idx 	= strval($this->_request->getParam('dlvr_mode_idx'));
            $this->view->cart 			= $this->_cartModel->getCartProducts();
            $this->_helper->_layout->setLayout('site/bootstrap-layout');
			$this->_helper->viewRenderer('index');
        }
        return true;
    }//-emptycartAction
	
	public function preorderAction()
    {
        $this->_helper->_layout->setLayout('site/bootstrap-layout');
        if(!is_array($this->view->formerrors)) {
            
            $this->view->formerrors = array();
        }
        if(!is_array($this->view->formvals)) {
            $this->view->formvals = array(
            	'fio' 		=> strval($this->_request->getParam('fio')),
                'email' 	=> strval($this->_request->getParam('email')),
                'phone' 	=> strval($this->_request->getParam('phone')),
                'index' 	=> strval($this->_request->getParam('index')),
                'city' 		=> strval($this->_request->getParam('city')),
                'address' 	=> strval($this->_request->getParam('address')),
                'comments' 	=> strval($this->_request->getParam('comments')),
            );
        }
		Zend_Captcha_Word::$CN = Zend_Captcha_Word::$C = Zend_Captcha_Word::$VN = Zend_Captcha_Word::$V = array("0","1","2","3","4","5","6","7","8","9");
        $this->view->captchaid = $this->_generateCaptcha();
        //Be as staticpage
        $this->getResponse()->setHeader('Expires', '', true);
        $this->getResponse()->setHeader('Cache-Control', 'private', true);
        $this->getResponse()->setHeader('Cache-Control', 'max-age=315360000');
        $this->getResponse()->setHeader('Pragma', '', true);
        $this->view->dlvr_mode_idx 	= intval($this->_request->getParam('dlvr_mode_idx'));
        $this->view->cart = $this->_cartModel->getCartProducts();
        return true;
    }//-preorderAction
	
	public function makeorderAction()
	{
        $this->_helper->_layout->setLayout('site/bootstrap-layout');

        $this->view->formvals = array(
            'fio' 		=> strval($this->_request->getParam('fio')),
            'email' 	=> strval($this->_request->getParam('email')),
            'phone' 	=> strval($this->_request->getParam('phone')),
            'city' 		=> strval($this->_request->getParam('city')),
            'index' 	=> strval($this->_request->getParam('index')),
            'address' 	=> strval($this->_request->getParam('address')),
            'comments' 	=> strval($this->_request->getParam('comments')),
            'dbg' 		=> strval(
                serialize($_REQUEST)
            ),
        );
        //srch
        $idx = strval($this->_request->getParam('ajax_srch_idx'));
        $cty = strval($this->_request->getParam('ajax_srch_cty'));
        $str = strval($this->_request->getParam('ajax_srch_str'));
        $dom = strval($this->_request->getParam('ajax_srch_dom'));
        $kva = strval($this->_request->getParam('ajax_srch_kva'));
        
        if(empty($this->view->formvals['city']) && !empty($cty)) {
            $this->view->formvals['city'] = $cty;
        }
        
        if(empty($this->view->formvals['address']) && !empty($str) && !empty($dom)){
            $this->view->formvals['address'] = $str . ' д. ' . $dom . ' кв. ' . $kva;
        }
        
        if(empty($this->view->formvals['index']) && !empty($idx)){
			$this->view->formvals['index'] = $idx;
        }
		$nf_reg 	= strval($this->_request->getParam('selectRegion'));
        $nf_ara 	= strval($this->_request->getParam('selectDistrict'));
        $nf_cty 	= strval($this->_request->getParam('selectSettlement'));
        $nf_str 	= strval($this->_request->getParam('selectStreet'));
        $nf_dom 	= strval($this->_request->getParam('HouseNumber'));
        $nf_domsub 	= strval($this->_request->getParam('HouseSubNumber'));
        $nf_kva 	= strval($this->_request->getParam('ApartmentNumber'));
        $nf_idx 	= strval($this->_request->getParam('IndexNumber'));
        $nf_addrstr = strval($this->_request->getParam('ResultAddressHidden'));
        if(	 empty( $this->view->formvals['city'] ) && !empty( $nf_reg )  ) {
            $this->view->formvals['city'] = $nf_reg;
        }
        
        if(  empty( $this->view->formvals['address'] ) && !empty( $nf_addrstr )  ) {
            $this->view->formvals['address'] = $nf_addrstr;
        }
        
        if(  empty( $this->view->formvals['index'] ) && !empty( $nf_idx )  ) {
            $this->view->formvals['index'] = $nf_idx;
        }
        $this->view->formerrors = array();
        if(  empty( $this->view->formvals['fio'] )  ) {
            $this->view->formerrors['fio'] = 1;
        }
		// fix10072013 requered email field
        if(  empty( $this->view->formvals['email'] )  ) {
        	$this->view->formerrors['email'] = 1;
        }
        if(  empty( $this->view->formvals['phone'] )  ) {
            $this->view->formerrors['phone'] = 1;
        }
        
        if(	 empty( $this->view->formvals['index'] )  ) {
            $this->view->formerrors['index'] = 1;
        }
		if($this->_validateCaptcha()){
            //
        }
        else{ 
            $this->view->formerrors['captcha'] = 1;
        }
        $dlvr_mode_idx = 
            $this->view->dlvr_mode_idx = 
                intval($this->_request->getParam('dlvr_mode_idx'));
		
		if( count($this->view->formerrors) ) {
			$this->view->formvals['dom'] = $dom;
            $this->view->formvals['kva'] = $kva;
            $this->view->formvals['str'] = $str;
			$this->view->formvals['nf_reg'] 	= $nf_reg;
            $this->view->formvals['nf_ara'] 	= $nf_ara;
            $this->view->formvals['nf_cty'] 	= $nf_cty;
            $this->view->formvals['nf_str'] 	= $nf_str;
            $this->view->formvals['nf_dom'] 	= $nf_dom;
            $this->view->formvals['nf_domsub'] 	= $nf_domsub;
            $this->view->formvals['nf_kva'] 	= $nf_kva;
            $this->view->formvals['nf_idx'] 	= $nf_idx;
            $this->view->formvals['nf_addrstr'] = $nf_addrstr;
            $this->view->captchaid 	= $this->_generateCaptcha();
            $this->view->cart 		= $this->_cartModel->getCartProducts();
            $this->_helper->viewRenderer('preorder');
			return true;
        }
        else {
            if( $store_responce = $this->_cartModel->StoreCartToOrder(
                $this->view->formvals,
                $dlvr_mode_idx, 
                array(
                    'nf_reg' => $nf_reg,
                    'nf_ara' => $nf_ara,
                    'nf_cty' => $nf_cty,
                    'nf_str' => $nf_str,
                    'nf_dom' => $nf_dom,
                    'nf_kva' => $nf_kva,
                    'nf_idx' => $nf_idx,
                    'nf_addrstr' => $nf_addrstr,
                )
            )){

                $acquiring_cfg = new Zend_Config_Ini('../application/configs/acquiring.ini', 'raiffeisen');

                $this->view->acquiring = array(
                    'PaymenentURL' 		=> $acquiring_cfg->PaymenentURL,
                    'CountryCode' 		=> $acquiring_cfg->CountryCode,
                    'CurrencyCode' 		=> $acquiring_cfg->CurrencyCode,
                    'MerchantName' 		=> $acquiring_cfg->MerchantName,
                    'MerchantURL' 		=> $acquiring_cfg->MerchantURL,
                    'MerchantCity' 		=> $acquiring_cfg->MerchantCity,
                    'MerchantID' 		=> $acquiring_cfg->MerchantID,
                    'PurchaseDesc' 		=> $store_responce['order_id'],
                    'PurchaseAmt' 	=> $store_responce['total'],
                    'SuccessURL' 	=> $acquiring_cfg->SuccessURL,
                    'FailURL' 	=> $acquiring_cfg->FailURL,
                    'Email' 	=> $acquiring_cfg->Email
                );
                /*
                 * Alextaran 23-11-12
                 * Отправляем письмо администратору и пользователю
                 */
				if(  strlen( $store_responce['fullorder']['contact']['email'] ) > 4  )
				{
					$this->sendmail2user($store_responce['fullorder']);
				}
				$this->sendmail2admin($store_responce['fullorder']);

				// Удаляем ненужный массив
                unset ($store_responce['fullorder']);

                //Be as staticpage
                $this->getResponse()->setHeader('Expires', '', true);
                $this->getResponse()->setHeader('Cache-Control', 'private', true);
                $this->getResponse()->setHeader('Cache-Control', 'max-age=315360000');
                $this->getResponse()->setHeader('Pragma', '', true);
            }
            else{
                $this->view->message = 'Корзина пуста';
                $this->_helper->viewRenderer('error');
            }
        }
        return true;
    }//-makeorderAction
	
	public function displayorderAction()
	{
        $order_id = strval($this->_request->getParam('ordernum'));
        $this->view->order_id = $order_id;
        if( !$this->_validateCaptcha() ) {
        	Zend_Captcha_Word::$CN = Zend_Captcha_Word::$C = Zend_Captcha_Word::$VN = Zend_Captcha_Word::$V = array("0","1","2","3","4","5","6","7","8","9");
            $this->view->captchaIsEntred = 0;
            $this->view->captchaid = $this->_generateCaptcha();
        }
        elseif( $this->view->order_data = $this->_cartModel->getOrder($order_id) ) {
                $this->view->captchaIsEntred = 1;
                $arts = array();
				/*
                 * AlexTaran 04.12.12
                 * Если клиент вернулся издалека и решил возобновить покупки,
                 * необходимо:
                 * 1) Проверить остатки по складе
                 * 2) Если чего-то нет, отказать в его оформлении
                 */
                foreach ( $this->view->order_data['order_products'] AS $product ) {
                	if ( $product['product_art'] == 'DLVR-001' ) {
                		continue;
                	}
                	$_CatalogModel = new Application_Model_CatalogModel();
                	if ( $_CatalogModel->CheckForAvalible($product['product_art']) ) {
                		continue;
                	}
                	else {
                		$this->view->outdated_order = 1;
                		break;
                		//$this->view->message = 'Ваш заказ устарел. Пожалуйста, оформите новый заказ.';
                		//$this->_helper->viewRenderer('error');
                	}
                }
                foreach(  array_keys( $this->view->order_data['order_products'] )  AS  $product  ) {
                    array_push(  $arts  ,  $this->view->order_data['order_products'][$product]['product_art'] . '*' . $this->view->order_data['order_products'][$product]['product_count']  );
                }
                $arts = implode(',', $arts);

                $acquiring_cfg = new Zend_Config_Ini('../application/configs/acquiring.ini', 'raiffeisen');

                $this->view->acquiring = array(
                	'PaymenentURL' 		=> $acquiring_cfg->PaymenentURL,
                    'CountryCode' 		=> $acquiring_cfg->CountryCode,
                    'CurrencyCode' 		=> $acquiring_cfg->CurrencyCode,
                    'MerchantName' 		=> $acquiring_cfg->MerchantName,
                    'MerchantURL' 		=> $acquiring_cfg->MerchantURL,
                    'MerchantCity' 		=> $acquiring_cfg->MerchantCity,
                    'MerchantID' 		=> $acquiring_cfg->MerchantID,
                    'PurchaseDesc' 		=> $this->view->order_data['order_contacts']['order_id'],
                    'PurchaseAmt' 	=> $this->view->order_data['order_contacts']['total'],
                    'SuccessURL' 	=> $acquiring_cfg->SuccessURL,
                    'SuccessURL' 	=> $acquiring_cfg->FailURL,
                    'Email' 	=> $acquiring_cfg->Email
                );
        }
        else {
            $this->view->message = 'Заказ не найден';
			$this->_helper->viewRenderer('error');
        }
        return true;
    }//-displayorderAction
    
    public function avAction()
    {
    	$order_id = strval($this->_request->getParam('ordernum'));
    	$this->view->order_id = $order_id;
    	 
    	if( $this->view->order_data = $this->_cartModel->getavOrder($order_id) ) {
    		$this->view->captchaIsEntred = 1;
    		$arts = array();
    		foreach ( $this->view->order_data['order_products'] AS $product ) {
    			if ( $product['product_art'] == 'DLVR-001' ) {
    				continue;
    			}
    			$_CatalogModel = new Application_Model_CatalogModel();
    			if ( $_CatalogModel->CheckForAvalible($product['product_art']) ) {
    				continue;
    			}
    			else {
    				$this->view->outdated_order = 1;
    				break;
    			}
    		}
    		foreach(  array_keys( $this->view->order_data['order_products'] )  AS  $product  ) {
    			array_push(  $arts  ,  $this->view->order_data['order_products'][$product]['product_art'] . '*' . $this->view->order_data['order_products'][$product]['product_count']  );
    		}
    		$arts = implode(',', $arts);
    		$rbcmoney_cfg = new Zend_Config_Ini('../application/configs/strikepro.ini', 'rbcmoney');

            $this->view->rbcmoney = array(
    				'payurl' 		=> $rbcmoney_cfg->rbc_payurl,
    				'eshopId' 		=> $rbcmoney_cfg->rbc_eshopId,
    				'orderId' 		=> $this->view->order_data['order_contacts']['order_id'],
    				'orderAmount' 	=> $this->view->order_data['order_contacts']['total'],
    				'currency' 		=> $rbcmoney_cfg->rbc_currency,
    				'successUrl' 	=> $rbcmoney_cfg->rbc_successUrl,
    				'failUrl' 		=> $rbcmoney_cfg->rbc_failUrl,
    				'product_arts' 	=> $arts,
    				'user_email' 	=> ((!empty($this->view->order_data['order_contacts']['email']))? $this->view->order_data['order_contacts']['email']:''),
    		);
    	}
    	else {
    		$this->view->message = 'Заказ не найден';
    		$this->_helper->viewRenderer('error');
    	}
    }
	
	public function paymentokAction()
	{
        //$this->_helper->_layout->setLayout('site/bootstrap-layout');
        return true;
    }//-paymentokAction
    
	public function paymenterrorAction()
	{
        //$this->_helper->_layout->setLayout('site/bootstrap-layout');
        return true;
    }//-paymenterrorAction
	
	public function paymentstoreAction()
	{
        $payment_data = array(
            'eshopId' 			=> strval($this->_request->getParam('eshopId')),
            'paymentId' 		=> strval($this->_request->getParam('paymentId')),
            'orderId' 			=> strval($this->_request->getParam('orderId')),
            'eshopAccount' 		=> strval($this->_request->getParam('eshopAccount')),
            'serviceName' 		=> strval($this->_request->getParam('serviceName')),
            'recipientAmount' 	=> strval($this->_request->getParam('recipientAmount')),
            'recipientCurrency' => strval($this->_request->getParam('recipientCurrency')),
            'paymentStatus' 	=> strval($this->_request->getParam('paymentStatus')),
            'userName' 			=> strval($this->_request->getParam('userName')),
            'userEmail' 		=> strval($this->_request->getParam('userEmail')),
            'paymentData' 		=> strval($this->_request->getParam('paymentData')),
            'secretKey' 		=> strval($this->_request->getParam('secretKey')),
            'hash' 				=> strval($this->_request->getParam('hash')),
            'userFieldS' 		=> strval(implode(' ', array_values(
                    $this->_get_suffixed_params('userField_')
                ))),
        );
		if($this->_cartModel->paymentStore($payment_data)) {
            //200 OK: compatable vth protocol v1 && v2
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->clearBody();
            $this->getResponse()->clearHeaders();            
            $this->getResponse()->setHttpResponseCode(200);
            $this->getResponse()->appendBody('OK');
			return true;
        }
        else{
            //404 NE: compatable vth protocol v1 && v2
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->clearBody();
            $this->getResponse()->clearHeaders();
            $this->getResponse()->setHttpResponseCode(404);
            $this->getResponse()->appendBody('FAIL');
            return true;
        }
        return true;
    }//-paymentstoreAction
	
	public function initnewcaptchaAction()
	{
        $captchaid = $this->_generateCaptcha();
        if($this->getRequest()->isXmlHttpRequest()){
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->json(
                array(
                    'newcid' => $captchaid,
                )
            );
        }
        else{
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->clearBody();
            $this->getResponse()->clearHeaders();            
            $this->getResponse()->setHttpResponseCode(200);
            $this->getResponse()->appendBody($captchaid);
        }
        return true;
    }//-initnewcaptchaAction    
	
	public function searchkladrAction()
    {
        $subaction = strval($this->_request->getParam('subaction'));
        $idx = strval($this->_request->getParam('idx'));
        $cty = strval($this->_request->getParam('cty'));
        $str = strval($this->_request->getParam('str'));
        $dom = strval($this->_request->getParam('dom'));
        $kladrobj = $this->_cartModel->getKladrResults($idx, $cty, $str, $dom, $subaction);
        if($this->getRequest()->isXmlHttpRequest()) {    
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->json(
                $kladrobj
            );
        }
        else {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->clearBody();
            $this->getResponse()->clearHeaders();            
            $this->getResponse()->setHttpResponseCode(200);
            $this->getResponse()->appendBody('AJAX ONLY');
        }
        return true;
    }//-searchkladrAction    
    
    protected function sendmail2user($order)
    {
        $message = '<p>Здравствуйте, ' . $order['contact']['fio'] . '!</p>
					<p>	Ваш заказ #' . $order['order_id'] . ' в интернет-магазине StrikePro.ru принят.<br>
					<p>Для того чтобы посмотреть или оплатить заказ перейдите по ссылке <a href="https://shop.strikepro.ru/order/' . $order['order_id'] . '">https://shop.strikepro.ru/order/' . $order['order_id'] . '</a></p>
					<p>По всем вопросам Вы можете связаться с менеджером магазина по телефону <b>+7 (981) 878 52 72</b> или ответив на это письмо.</p>
				
					<h2> Ваш заказ: </h2>
					<table cellpadding="0" cellspacing="0" style="border-collapse:collapse">
						
						<th style="width:160px">Артикул</th>
						<th style="width:219px">Название</th>
						<th style="width:160px">Кол-во</th>
						<th style="width:160px">Цена</th>
						<th style="width:100px">Сумма</th>';
						
        			foreach( $order['goodies']  AS  $position  ) :
        				$message .= '<tr>
							<td style="border:1px solid silver;padding:3px;width:100px;vertical-align:middle;text-align:center">' . $position['product_art'] . '</td>
							<td style="border:1px solid silver;padding:3px;vertical-align:middle;width:219px;text-align:center">' .  $position['product_name'] . '</td>
							<td style="border:1px solid silver;padding:3px;vertical-align:middle;text-align:center">' .  $position['product_count'] . '</td>
							<td style="border:1px solid silver;padding:3px;width:100px;vertical-align:middle;text-align:right;white-space:nowrap;text-align:center">' . $position['product_price'] . '</td>
							<td style="border:1px solid silver;padding:3px;width:100px;padding-left:10px;vertical-align:middle;white-space:nowrap;text-align:right">' . $position['product_total'] . '</td>
						</tr>';
					endforeach;
		$message .= '<tr>
					<td colspan="4" style="text-align:right;"><b>'. $order['delivery']['product_name'] .'</b></td>
					<td style="text-align:right;font-weight:bold;padding:4px;white-space:nowrap">' . $order['delivery']['product_total'] . '</td>
					</tr>';
		$message .= '<tr>
						<td colspan="4" style="text-align:right;"><b>Итого:</b></td>
							<td style="text-align:right;font-weight:bold;padding:4px;white-space:nowrap">' . $order['total'] . '</td>
					</tr>
					
					</table>';
		$message.='<p>Ваш заказ будет отправлен по адресу: <b>'.$order['contact']['address'].'</b></p>';
		$message.='<p>Ваш коментарий к заказу: '.$order['contact']['comments'].'</p>';
		$message.='<p style="color:grey"><i>C уважением, команда StrikePro</i></p>';
		$config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
        $tr_conf = array(
                    'auth' => $config->mail->auth,
                    'port' => $config->mail->port,
                    'ssl'   => $config->mail->ssl,
                    'username' => $config->mail->username,
                    'password' => $config->mail->password);
        
        $transport = new Zend_Mail_Transport_Smtp('mail.strikepro.ru', $tr_conf);
        $mail = new Zend_Mail('UTF-8');
        $mail->setBodyHtml($message);
        $mail->setReplyTo('shop@strikepro.ru', 'StrikePro.ru');
        $mail->setFrom('no-reply@strikepro.ru', 'StrikePro.ru');
        $mail->addTo( $order['contact']['email']  ,   $order['contact']['fio'] );
        $mail->setSubject('Ваш заказ #' . $order['order_id'] . ' на сайте StrikePro.ru принят');
        $mail->send($transport);    
    
    
    } // EOF sendmail2user
    
    protected function sendmail2admin($order)
    {
		$csv_splitter= ',';
        $csv_order = '';
		$message = 'StrikePro.Ru Заказ <a href="https://shop.strikepro.ru/av/'. $order['order_id'] .'">#'. $order['order_id'] . "</a>\n";
		$message .= '<table cellpadding="0" cellspacing="0" style="border-collapse:collapse">
	        			<th style="width:160px">Артикул</th>
	        			<th style="width:219px">Название</th>
	        			<th style="width:160px">Кол-во</th>
	        			<th style="width:160px">Цена</th>
	        			<th style="width:100px">Сумма</th>';
        foreach( $order['goodies']  AS  $position  ) :
        	$message .= 	'<tr>
							<td style="border:1px solid silver;padding:3px;width:100px;vertical-align:middle;text-align:center">' . $position['product_art'] . '</td>
							<td style="border:1px solid silver;padding:3px;vertical-align:middle;width:219px;text-align:center">' .  $position['product_name'] . '</td>
							<td style="border:1px solid silver;padding:3px;vertical-align:middle;text-align:center">' .  $position['product_count'] . '</td>
							<td style="border:1px solid silver;padding:3px;width:100px;vertical-align:middle;text-align:right;white-space:nowrap;text-align:center">' . $position['product_price'] . '</td>
							<td style="border:1px solid silver;padding:3px;width:100px;padding-left:10px;vertical-align:middle;white-space:nowrap;text-align:right">' . $position['product_total'] . '</td>
						</tr>';
        	$csv_order .= 	$position['product_art'] . $csv_splitter . $position['product_count'] . "\n";
        endforeach;
		$message .= '<tr>
					<td colspan="4" style="text-align:right;"><b>'. $order['delivery']['product_name'] .'</b></td>
					<td style="text-align:right;font-weight:bold;padding:4px;white-space:nowrap">' . $order['delivery']['product_total'] . '</td>
					</tr>';
		$message .= '<tr>
						<td colspan="4" style="text-align:right;"><b>Итого:</b></td>
							<td style="text-align:right;font-weight:bold;padding:4px;white-space:nowrap">' . $order['total'] . '</td>
					</tr>
					</table>';
		$message.='<p>Куда: <b>'.$order['contact']['address'].'</b></p>';
		$csv_order = $csv_order. 'Номер Заказа: ' . $order['order_id'] . "\n";
        $csv_order = $csv_order. 'ФИО: ' . $order['contact']['fio'] . "\n";
        $csv_order = $csv_order. 'Email: ' .$order['contact']['email'] . "\n";
        $csv_order = $csv_order. 'Телефон: ' . $order['contact']['phone'] . "\n";
        $csv_order = $csv_order. 'Aдрес Доставки: ' . $order['contact']['address'] . "\n";
        $csv_order = $csv_order. 'Комментарии к заказу: ' . $order['contact']['comments'] . "\n";
		
        $config = new Zend_Config_Ini('../application/configs/application.ini', 'production');
        $tr_conf = array(
                    'auth' => $config->mail->auth,
                    'port' => $config->mail->port,
                    'ssl'   => $config->mail->ssl,
                    'username' => $config->mail->username,
                    'password' => $config->mail->password);

		$transport = new Zend_Mail_Transport_Smtp('mail.strikepro.ru', $tr_conf);
		$mail = new Zend_Mail('UTF-8');
        $mail->setBodyHtml($message);
        $attscv = $mail->createAttachment($csv_order,
        		'text/csv',
        		Zend_Mime::DISPOSITION_ATTACHMENT,
        		Zend_Mime::ENCODING_BASE64);
        $attscv->filename = $order['order_id'] . '.csv';
        $mail->setFrom('no-reply@strikepro.ru', 'StrikePro.ru');
        $mail->addTo('shop@strikepro.ru', 'Strike Pro Online Store');
        $mail->setSubject('StrikePro: заказ #' . $order['order_id']);
        $mail->send($transport);
    } // EOF sendmail2admin


    //get array nums=values suffexed with \d+ rownum: pram_1, param_2
    protected function _get_suffixed_params($param_base = false)
    {
    	if(empty($param_base)){
    		return array();
    	}
    	$out = array();
    	$params = $this->_request->getParams();
    	$param_regexp = preg_quote($param_base);
    	$param_regexp = "/^$param_regexp(\d+)/";
    	foreach (array_keys($params) as $param) {
    		$matches = array();
    		if( preg_match($param_regexp, $param, $matches)  ) {
    			$out[$matches[1]] = $params[$param];
    		}
    	}
    	return $out;
    }//-_get_suffixed_params
    
    protected function _generateCaptcha()
    {
    	$config = new Zend_Config_Ini('configs/application.ini', 'production');
    	$captcha = new Zend_Captcha_Image();
    	$captcha->setTimeout(30*60)//min
    	->setName('captcha_id')
    	->setWordLen('4')
    	->setWidth('200')
    	->setHeight('100')
    	->setFontSize('48')
    	->setUseNumbers('1')
    	->setDotNoiseLevel('15')
    	->setLineNoiseLevel('1')
    	->setFont((APPLICATION_PATH).'/fonts/DejaVuSansMono.ttf')
    	->setImgUrl('/images/captchas')
    	->setSuffix('.png')
    	//->setSession(new Zend_Session_Namespace('Zend_Form_Captcha_rnd'))
    	->setImgDir(($config->basepath).'/images/captchas');
    	$captcha->generate();    //command to generate session + create image
    	$formfield = $captcha->render();
    	return $captcha->getId();   //returns the ID given to session & image
    }//- _generateCaptcha
    
    protected function _validateCaptcha()
    {
    	if ( $_SERVER['REMOTE_ADDR'] == '89.112.5.1' OR $_SERVER['REMOTE_ADDR'] == '95.161.19.29' ) return true; // заплатка для Ромы
    	$captcha = new Zend_Captcha_Image();
    	if ( array_key_exists('captcha', $_POST) && $captcha->isValid($_POST['captcha']) ) {
    		return true;
    	}
    	return false;
    }//- _validateCaptcha
}// EOC
