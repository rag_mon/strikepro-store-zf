/*WSS*/

var testobj = false;
var ajax_indicator_counter = 0;

function create__basket() {
    $.ajax({
        type : 'GET',
        url : '/cart/getajaxproducts',
        success : function(data)
        {
            if(data.number_of_goods_in_cart != 0){
                $(".basket__count,.basket__content").show();
                $(".basket__notic").hide();
                $("#count-item").text(data.number_of_goods_in_cart);
                $(".count-item").text(data.number_of_goods_in_cart);
                $(".basket__count-totals__price").text(data.cost_of_goods);
                create_content_popup_basket(data.goods_list);
            } else {
                $(".basket__count,.basket__content").hide();
                $(".basket__notic").show();
            }
        }
    });
}


function create_content_popup_basket(array) {
    $("#js-basket__content").empty();
    var length__array = array.length;
    for(var i = 0; i < length__array; i++){
        var template = "<tr><td >"+
            array[i].goods_name
            +"</td><td>"+
            check_str(array[i].goods_price)+'<i class="fa fa-rub" aria-hidden="true"></i>'
            +"</td><td>"+
            array[i].number_of_goods
            +"</td><td>"+
            array[i].total_cost_goods+'<i class="fa fa-rub" aria-hidden="true"></i>'
            +"</td></tr>";
        $("#js-basket__content").append(template);
    }

}

function check_str(str) {
    return str.substring(0, str.indexOf(".") + 3);
}



$(document).ready(function() {
    if($('#ajax_indicator_canvas').length){
        jQuery(document).ajaxStart(function(e, xhr, settings) {
            ajax_indicator_counter++;
            $('#ajax_indicator_canvas').removeClass('iv');
        });
        jQuery(document).ajaxComplete(function(e, xhr, settings) {
            ajax_indicator_counter--;
            if(ajax_indicator_counter<1){
                $('#ajax_indicator_canvas').addClass('iv');
            }
        });
    }

    create__basket();
});

function randy_by_len(len) {
    var rnd_seq='';
    var step='';
    var rdigit='';
    for(step=0; step<len; step++)
      {
        rdigit=Math.floor(Math.random()*10);
        rnd_seq = rnd_seq + rdigit+"";
      }
  return rnd_seq;
}//-randy_by_len


function addToCart(formobj){

    $.ajax({
        type : 'POST',
        url : ($('head').data('site_url'))+$(formobj).attr('action'),
        data : $(formobj).serialize(),
        success : function(msg)
        {
            if( msg.status == 'ok')
            {
                //$('#popup').attr('data-content', msg.cartblock);
                //$('#count-item').text(msg.countitem);
                create__basket();
                console.log(msg);
                $.jGrowl("Товар добавлен в корзину:<br />"+ msg.product.fullname_fmt, {
                         speed: 'slow'
                });
            }
            else{
                alert(msg.status);
            }
        }
    });
    return false;
}//-addToCart


function rmFromCart(row, art){
    $.ajax({
        type : 'GET',
        url : ($('head').data('site_url'))+'/cart/rmproduct',
        data : 
        {
            'rnd':randy_by_len(6),
            'art':art
        },
        success : function(msg) {
            if(msg.status=='ok') {
                $('#product_row_'+row).remove();
                
                var rowrecnt=1;
                $('#cart_products_table tr').each(function(pos, trobj){
                    td = $(trobj).find('td.rownumtd').first();
                    if($(td).hasClass('rownumtd')){td.text(rowrecnt);rowrecnt++;}
                });
                
                if(parseInt(msg.rawcart.deliverycosts)==0){
                    $('#cart_products_delivery').addClass('g');
                    $('#cart_products_delivery').text('бесплатно');
                }
                else{
                    $('#cart_products_delivery').removeClass('g');
                    $('#cart_products_delivery').text(msg.rawcart.deliverycosts_fmt);
                }
                $('#cart_products_summ').text(msg.rawcart.total_fmt_w_delivery);
                
                $('#cartblock_countarts').text(msg.rawcart.countarts);
                $('#cartblock_countitems').text(msg.rawcart.countitems);
                $('#cartblock_summ').text(msg.rawcart.total_fmt);
                
                $.jGrowl('Товар удален из корзины:<br />'+msg.product.fullname_fmt, { 
                         speed:  'slow'
                });
            }
            else{
                alert('Ошибка AJAX запроса');
            }
        }
    });
    
    return false;
    
}//-rmFromCart

function askEmptyCart(){
    if(confirm('Очистить корзину?')) {
        $.ajax({
            type: 'POST',
            url: ($('head').data('site_url')) + '/cart/emptycart',
            data: {
            },
            success: function (msg) {
                if (msg.status == 'ok') {
                    document.location = $('head').data('site_url') + '/cart';
                }
                else{
                    alert('Ошибка AJAX запроса');
                }
            }
        });
    }
    return false;
}//-askEmptyCart


function setProdQty(art, qty){

    var ajax_rep=$.ajax({
        type : "GET",
        url : ($('head').data('site_url'))+'/cart/setquantity',
        contentType : "text/html",
        dataType : 'html',
        global: false,
        async:false,
        data : {
            'rnd':randy_by_len(6),
            'incl_cart_stats':1,
            'art_row_1':art,
            'qty_row_1':qty
        }
    }).responseText;
    
    if(ajax_rep) {
        return $.parseJSON(ajax_rep);
    }
    else {
        return false;
    }
    
}//-askEmptyCart


function updCaptcha(aobj){
    
    var ajax_rep=$.ajax({
        type : "GET",
        url : ($('head').data('site_url'))+'/cart/initnewcaptcha',
        contentType : "text/html",
        dataType : 'html',
        global: false,
        async:false,
        data : {
            'rnd':randy_by_len(6)
        }
    }).responseText;
    
    if(ajax_rep){
        $.jGrowl('Обновляем капчу', {
                 speed:  'fast'
        });
        ajax_rep = $.parseJSON(ajax_rep);
        $(aobj).find('img').attr('src', '/images/captchas/'+(ajax_rep.newcid)+'.png?rnd='+(randy_by_len(6)));
        $(aobj).parent('div').find('input[name=captcha\\[id\\]]').val(ajax_rep.newcid);
    }
    else{
        $.jGrowl('Ошибка обновления капчи', {
                 speed:  'slow'
        });
    }
    
        return false;
}//-updCaptcha

function searcKladr_orig(aobj){
    
    var idx = $('#ajax_srch_idx').val();
    var cty = $('#ajax_srch_cty').val();
    var str = $('#ajax_srch_str').val();
    var dom = $('#ajax_srch_dom').val();
    
    var errmsg = '';
    if(!cty.length){
        $('#ajax_srch_cty').addClass('ni');
        errmsg += 'Введите город, пожалуйста.\n';
    }
    else{
        $('#ajax_srch_cty').removeClass('ni');
    }
    if(!str.length){
        $('#ajax_srch_str').addClass('ni');
        errmsg += 'Введите улицу, пожалуйста.\n';
    }
    else{
        $('#ajax_srch_str').removeClass('ni');
    }
    
    if(!$('#preorderform input[name=city]').val().length){
        $('#preorderform input[name=city]').val($('#ajax_srch_cty').val());
    }
    
    if(!$('#preorderform input[name=address]').val().length){
        $('#preorderform input[name=address]').val($('#ajax_srch_str').val()+' д.'+$('#ajax_srch_dom').val());
    }
    
    
    if(errmsg.length){
        alert(errmsg);
        return false;
    }
    
    var ajax_rep=$.ajax({
        type : "GET",
        url : ($('head').data('site_url'))+'/cart/searchkladr',
        contentType : "text/html",
        dataType : 'html',
        global: false,
        async:false,
        data : {
            'rnd':randy_by_len(6),
            'idx':idx,
            'cty':cty,
            'str':str,
            'dom':dom
            
        }
    }).responseText;
    
    if(ajax_rep){
        $.jGrowl('Обновляем варианты адресов', {
                 speed:  'fast'
        });
        
        testobj = ajax_rep = $.parseJSON(ajax_rep);
        
        $('#ajax_srch_idx_out').text('');
        if(ajax_rep.idxs && typeof(ajax_rep.idxs)=="object"){
            for (i=0;i<ajax_rep.idxs.length;i++){
                if(parseInt(ajax_rep.idxs[i].index)){
                    $('#ajax_srch_idx_out').html(
                        $('#ajax_srch_idx_out').html()
                        +
                        '<a onClick="javascript:kladrPasteIdx(\''+ajax_rep.idxs[i].index+'\');return false;" class="b u hp nwp" title="подставить индекс" href="#">'
                        +
                        ajax_rep.idxs[i].index
                        +
                        '</a> '
                        +
                        ajax_rep.idxs[i].name
                        +
                        ' '
                        +
                        ajax_rep.idxs[i].socr
                        +
                        '.<br />'
                    );
                }
                else{
                    $('#ajax_srch_idx_out').html(
                        $('#ajax_srch_idx_out').html()
                        +
                        '<b>-------</b> '
                        +
                        ajax_rep.idxs[i].name
                        +
                        ' '
                        +
                        ajax_rep.idxs[i].socr
                        +
                        '.<br />'
                    );
                }
            }
        }
        
        $('#ajax_srch_cty_out').text('');
        if(ajax_rep.ctys && typeof(ajax_rep.ctys)=="object"){
            for (i=0;i<ajax_rep.ctys.length;i++){
                if(parseInt(ajax_rep.ctys[i].index)){
                    $('#ajax_srch_cty_out').html(
                        $('#ajax_srch_cty_out').html()
                        +
                        '<a onClick="javascript:kladrPasteIdx(\''+ajax_rep.ctys[i].index+'\');return false;" class="b u hp nwp" title="подставить индекс" href="#">'
                        +
                        ajax_rep.ctys[i].index
                        +
                        '</a> '
                        +
                        ajax_rep.ctys[i].socr
                        +
                        '. '
                        +
                        ajax_rep.ctys[i].name
                        +
                        '<br />'
                    );
                }
                else{
                    $('#ajax_srch_cty_out').html(
                        $('#ajax_srch_cty_out').html()
                        +
                        '<b>-------</b> '
                        +
                        ajax_rep.ctys[i].socr
                        +
                        '. '
                        +
                        ajax_rep.ctys[i].name
                        +
                        '<br />'
                    );
                }
            }
        }
        
        $('#ajax_srch_str_out').text('');
        if(ajax_rep.strs && typeof(ajax_rep.strs)=="object"){
            for (i=0;i<ajax_rep.strs.length;i++){
                if(parseInt(ajax_rep.strs[i].index)){
                    $('#ajax_srch_str_out').html(
                        $('#ajax_srch_str_out').html()
                        +
                        '<a onClick="javascript:kladrPasteIdx(\''+ajax_rep.strs[i].index+'\');return false;" class="b u hp nwp" title="подставить индекс" href="#">'
                        +
                        ajax_rep.strs[i].index
                        +
                        '</a> '
                        +
                        ajax_rep.strs[i].name
                        +
                        ' '
                        +
                        ajax_rep.strs[i].socr
                        +
                        '.<br />'
                    );
                }
                else{
                    $('#ajax_srch_str_out').html(
                        $('#ajax_srch_str_out').html()
                        +
                        '<b>-------</b> '
                        +
                        ajax_rep.strs[i].name
                        +
                        '. '
                        +
                        ajax_rep.strs[i].socr
                        +
                        '<br />'
                    );
                }
                if(typeof(ajax_rep.doms['c'+ajax_rep.strs[i].code])=="object" && ajax_rep.doms['c'+ajax_rep.strs[i].code].length){
                    for (d=0;d<ajax_rep.doms['c'+ajax_rep.strs[i].code].length;d++){
                        $('#ajax_srch_str_out').html(
                            $('#ajax_srch_str_out').html()
                            +
                            '&nbsp;&nbsp;&nbsp;&nbsp;<a onClick="javascript:kladrPasteIdx(\''+ajax_rep.doms['c'+ajax_rep.strs[i].code][d].index+'\');return false;" class="b u hp nwp" title="подставить индекс" href="#">'
                            +
                            ajax_rep.doms['c'+ajax_rep.strs[i].code][d].index
                            +
                            '</a> '
                            +
                            ajax_rep.strs[i].name
                            +
                            ' '
                            +
                            ajax_rep.strs[i].socr
                            +
                            '.,  '
                            +
                            ajax_rep.doms['c'+ajax_rep.strs[i].code][d].name
                            +
                            ' '
                            +
                            ajax_rep.doms['c'+ajax_rep.strs[i].code][d].socr
                            +
                            '.<br />'
                        );
                    }
                }
            }
        }
        
    }
    else{
        $.jGrowl('Ошибка получения данных с сервера', {
                 speed:  'slow'
        });
    }
    
    return false;
    
}//-searcKladr_orig


function kladrPasteIdx(idx){
    $('#orderform_index').val(idx);
    $.jGrowl('Индекс подставлен', {
             speed:  'slow'
    });
}//-updCaptcha


function touchoption(e){
    
    
    var foundvalue = '';
    var selParent = '';
    if($(e.target)[0].tagName.toUpperCase()=='SELECT'){
        foundvalue = $(e.target).find('option:selected').val();
        selParent = $($(e.target)).attr('name');
    }
    else{
        foundvalue = $(e.target).val();
        selParent = $($(e.target)).parent().attr('name');
    }
    
    if(!foundvalue || foundvalue=='0'){
        e.preventDefault();
        return false;
    }
    
    var foundvalueArr = foundvalue.split('|');
    var foundindex = parseInt(foundvalueArr[0]);
    if(foundindex){
        $('#orderform_index').val(foundindex);
    }
    else{
        //alert('Индекс для поля не задан');
        e.preventDefault();
        return false;
    }
    
    
    if(selParent=='street_ajax_select'){
        //str
        if(foundvalueArr.length==5){
            //numbers: index,name,socr,building,bsocr
            $('#ajax_srch_str').val(
                foundvalueArr[1]
                +
                ' '
                +
                foundvalueArr[2]
                /* +
                '., '
                +
                foundvalueArr[3]
                +
                ' '
                +
                foundvalueArr[4]*/
                +
                '.'
                
            );
        }
        else{
            //just str: index,name,socr
            $('#ajax_srch_str').val(
                foundvalueArr[1]
                +
                ' '
                +
                foundvalueArr[2]
                +
                '.'
                
            );
        }
    }
    else if(selParent=='city_ajax_select'){
        //cty: index,socr,name
            $('#ajax_srch_cty').val(
                foundvalueArr[2]
            );
            
    }
}//-touchoption

function searchKladr(aobj, chkmode){
    
    var fieldminlen = parseInt(5);
    
    var idx = $('#ajax_srch_idx').val();
    
    var cty = $('#ajax_srch_cty').val();
    var selcty = $('#preorderform select[name=city_ajax_select] option:selected').val();
    
    var str = $('#ajax_srch_str').val();
    var selstr = $('#preorderform select[name=street_ajax_select] option:selected').val();
    
    var dom = $('#ajax_srch_dom').val();
    
    var errmsg = '';
    
    
    if(!cty.length && chkmode=='cty'){
        $('#ajax_srch_cty').addClass('ni');
        errmsg += 'Введите город, пожалуйста.\n';
    }
    else if(chkmode=='cty'){
        $('#ajax_srch_cty').removeClass('ni');
    }
    
    if(!str.length && chkmode=='str'){
        $('#ajax_srch_str').addClass('ni');
        errmsg += 'Введите улицу, пожалуйста.\n';
    }
    else if(chkmode=='str'){
        $('#ajax_srch_str').removeClass('ni');
    }
    
    
    if(errmsg.length){
        alert(errmsg);
        return false;
    }
    
    if(cty.length<fieldminlen){
       cty = ''; 
    }
    
    if(str.length<fieldminlen){
       str = ''; 
    }
    
    var ajax_rep= '';
    if(cty.length || str.length){
        ajax_rep=$.ajax({
            type : "GET",
            url : ($('head').data('site_url'))+'/cart/searchkladr',
            contentType : "text/html",
            dataType : 'html',
            global: false,
            async:false,
            data : {
                'rnd':randy_by_len(6),
                'idx':idx,
                'cty':cty,
                'selcty':selcty,
                'str':str,
                'selstr':selstr,
                'dom':dom
                
            }
        }).responseText;
    }
    
    if(ajax_rep && (cty.length || str.length)){
        $.jGrowl('Обновляем варианты адресов', {
                 speed:  'fast'
        });
        
        ajax_rep = $.parseJSON(ajax_rep);
        
        
        $('#ajax_srch_addr_out').text('');
        $('#ajax_srch_addrcty_out').text('');
        $('#ajax_srch_addrstr_out').text('');
        var haveresults = 0;
        
        
        
        if(ajax_rep.ctys && typeof(ajax_rep.ctys)=="object" && ajax_rep.ctys.length){
            //$('#ajax_srch_addrcty_out').append($('<span><\/span>').html('Город(а): ').addClass('lual'));
            $('#ajax_srch_addrcty_out').append($('<select><\/select>').attr('name', 'city_ajax_select').attr('size', '6'));
            $('#ajax_srch_addrcty_out').append($('<br><\/br>'));

            for (i=0;i<ajax_rep.ctys.length;i++){
                if(parseInt(ajax_rep.ctys[i].index)){
                    
                    haveresults = ''+ajax_rep.ctys[i].index;
                    
                    $('#ajax_srch_addrcty_out select[name=city_ajax_select]').append(
                        $('<option><\/option>').val(
                            ajax_rep.ctys[i].index
                            +
                            '|'
                            +
                            ajax_rep.ctys[i].socr
                            +
                            '|'
                            +
                            ajax_rep.ctys[i].name
                            
                        ).html(
                            ajax_rep.ctys[i].index
                            +
                            ' '
                            +
                            ajax_rep.ctys[i].socr
                            +
                            '. '
                            +
                            ajax_rep.ctys[i].name
                        )
                    );
                }
                else{
                    $('#ajax_srch_addrcty_out select[name=city_ajax_select]').append(
                        $('<option><\/option>').val(
                            '-------'
                            +
                            '|'
                            +
                            ajax_rep.ctys[i].socr
                            +
                            '|'
                            +
                            ajax_rep.ctys[i].name
                            
                        ).html(
                            '-------'
                            +
                            ' '
                            +
                            ajax_rep.ctys[i].socr
                            +
                            '. '
                            +
                            ajax_rep.ctys[i].name
                        )
                    );
                }
            }

            $('#ajax_srch_addrcty_out select[name=city_ajax_select] option').first().attr('selected', true);

            if($('#ajax_srch_addrcty_out select[name=city_ajax_select] option').length = 1 && !(parseInt($('#preorderform input[name=index]').val()))){
                $('#preorderform input[name=index]').val(haveresults);
            }
        }
        else{
            $('#ajax_srch_addrcty_out').html(
                $('#ajax_srch_addrcty_out').html()
                    +
                //'<b>Город(а) не найден(ы). Улицы не искались.</b><br />'
                '<select name="street_ajax_select" size="6"><option value="0" selected="selected">Город(а) не найден(ы). Улицы не искались.</option></select>'
            );
        }
        
        if(ajax_rep.strs && typeof(ajax_rep.strs)=="object" && ajax_rep.strs.length){
            //$('#ajax_srch_addrstr_out').append($('<span><\/span>').html('Улиц(-а/-ы): ').addClass('lual'));
            $('#ajax_srch_addrstr_out').append($('<select><\/select>').attr('name', 'street_ajax_select').attr('size', '6'));
            $('#ajax_srch_addrstr_out').append($('<br><\/br>'));
            
            for (i=0;i<ajax_rep.strs.length;i++){
                if(parseInt(ajax_rep.strs[i].index)){
                    
                    haveresults = ''+ajax_rep.strs[i].index;
                    
                    $('#ajax_srch_addrstr_out select[name=street_ajax_select]').append(
                        $('<option><\/option>').val(
                            ajax_rep.strs[i].index
                            +
                            '|'
                            +
                            ajax_rep.strs[i].name
                            +
                            '|'
                            +
                            ajax_rep.strs[i].socr
                            
                        ).html(
                            ajax_rep.strs[i].index
                            +
                            ' '
                            +
                            ajax_rep.strs[i].socr
                            +
                            '. '
                            +
                            ajax_rep.strs[i].name
                        )
                    );
                }
                else if(!dom){
                    $('#ajax_srch_addrstr_out select[name=street_ajax_select]').append(
                        $('<option><\/option>').val(
                            '-------'
                            +
                            '|'
                            +
                            ajax_rep.strs[i].name
                            +
                            '|'
                            +
                            ajax_rep.strs[i].socr
                            
                        ).html(
                            '-------'
                            +
                            ' '
                            +
                            ajax_rep.strs[i].socr
                            +
                            '. '
                            +
                            ajax_rep.strs[i].name
                        )
                    );
                }
                if(
                    dom &&
                    typeof(ajax_rep.doms['c'+ajax_rep.strs[i].code])=="object" && 
                    ajax_rep.doms['c'+ajax_rep.strs[i].code].length
                ){
                    for (d=0;d<ajax_rep.doms['c'+ajax_rep.strs[i].code].length;d++){
                        
                        haveresults = ''+ajax_rep.doms['c'+ajax_rep.strs[i].code][d].index;
                        
                        $('#ajax_srch_addrstr_out select[name=street_ajax_select]').append(
                            $('<option><\/option>').val(
                                ajax_rep.doms['c'+ajax_rep.strs[i].code][d].index
                                +
                                '|'
                                +
                                ajax_rep.strs[i].name
                                +
                                '|'
                                +
                                ajax_rep.strs[i].socr
                                +
                                '|'
                                +
                                ajax_rep.doms['c'+ajax_rep.strs[i].code][d].name
                                +
                                '|'
                                +
                                ajax_rep.doms['c'+ajax_rep.strs[i].code][d].socr
                                
                            ).html(
                                ajax_rep.doms['c'+ajax_rep.strs[i].code][d].index
                                +
                                ' '
                                +
                                ajax_rep.strs[i].name
                                +
                                ' '
                                +
                                ajax_rep.strs[i].socr
                                +/*
                                '.,  '
                                +
                                ajax_rep.doms['c'+ajax_rep.strs[i].code][d].name
                                +
                                ' '
                                +
                                ajax_rep.doms['c'+ajax_rep.strs[i].code][d].socr
                                +*/
                                '.'
                            )
                        );
                    }
                }
            }
            $('#ajax_srch_addrstr_out select[name=street_ajax_select] option').first().attr('selected', true);
            //$('#ajax_srch_addrstr_out select[name=street_ajax_select] option').first().click();
            if($('#ajax_srch_addrstr_out select[name=street_ajax_select] option').length = 1){
                $('#preorderform input[name=index]').val(haveresults);
           }
        }
        else{
            $('#ajax_srch_addrstr_out').html(
                $('#ajax_srch_addrstr_out').html()
                    +
                //'<b>Улиц(-а/-ы) не найден(-а/-ы)</b><br />'
                '<select name="street_ajax_select" size="6"><option value="0" selected="selected">Улиц(-а/-ы) не найден(-а/-ы)</option></select>'
            );
        }

        if(haveresults){
            /*$('#ajax_srch_addr_out').html(
                '<br /><u>Двойной щелчек мышью на поле для подстановки</u><br /><br />'
                +
                $('#ajax_srch_addr_out').html()
            );*/
            
            $('#preorderform option').click(function(e) {
                return touchoption(e);
            });
            
            $('#preorderform select').change(function(e) {
                return touchoption(e);
            });
            
            $('#preorderform option').keypress(function(e) {
                return touchoption(e);
            });
            
        }
        $('#ajax_srch_addrcty_out').append($('<br><\/br>'));
        $('#ajax_srch_addrstr_out').append($('<br><\/br>'));
        
    }
    else if(!ajax_rep && (cty.length>=fieldminlen || str.length>=fieldminlen)){
        $.jGrowl('Ошибка получения данных с сервера', {
                 speed:  'slow'
        });
    }
    else{
        
    }
    
    return false;
    
}//-searchKladr

function preorderformChk(){
	var email = $('#orderform_email').val();
    var idx = $('#IndexNumber').val();
    /*var cty = $('#ajax_srch_cty').val();
    var str = $('#ajax_srch_str').val();*/
    var dom = $('#HouseNumber').val();
    var kva = $('#ApartmentNumber').val();

    var errmsg = '';
    
    //alextaran
    if(!email.length)
    {
    	$('#orderform_email').addClass('ni');
        errmsg += 'Пожалуйста, введите адрес электронной почты.\n';
    }
    else {
    	var atpos=email.indexOf("@");
    	var dotpos=email.lastIndexOf(".");
    	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
    		$('#orderform_email').addClass('ni');
    		errmsg += 'Пожалуйста, введите правильно адрес электронной почты.\n';
    	}
    	else {
    		$('#orderform_email').removeClass('ni');
    	}
    }
    // end of alextaran
    if(!idx.length){
        $('#IndexNumber').addClass('ni');
        errmsg += 'Введите индекс, пожалуйста.\n';
    }
    else{
        $('#IndexNumber').removeClass('ni');
    }

    if(!dom.length){
        $('#HouseNumber').addClass('ni');
        errmsg += 'Введите дом, пожалуйста.\n';
    }
    else{
        $('#HouseNumber').removeClass('ni');
    }
    
    if(!kva.length){
        $('#ApartmentNumber').addClass('ni');
        errmsg += 'Введите квартиру, пожалуйста.\n';
    }
    else{
        $('#ApartmentNumber').removeClass('ni');
    }
    
    
    if(errmsg.length){
        
    	alert(errmsg);
        return false;
    }
    
    return true;
}//-preorderformChk


function FilterAlphaNumeric(keyCode)
  { 
    if((57>=keyCode && keyCode>=48) 
        || (105>=keyCode && keyCode>=96) 
        || (90>=keyCode && keyCode>=65) 
        || (40>=keyCode && keyCode>=37)
        || (keyCode == 8) || (keyCode == 46)  
        || (keyCode == 13) || (keyCode == 32))
    {
        return true;
    }
    else
    {
        return false;
    }
  }//-FilterAlphaNumeric
