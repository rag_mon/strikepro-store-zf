<?php

class App_Validate_DateTime extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
        $pattern = '/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/';
        if (preg_match($pattern, $value)) {
            return true;
        }
        else {
            return false;
        }
    }
}