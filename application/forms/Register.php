<?php
class Application_Form_Register extends Zend_Form
{
	public function init()
    {
        // Вызываем родительский метод
        parent::init();
        
        $this->addElementPrefixPath('App_Validate', 'App/Validate/', 'validate');
        $this->addElementPrefixPath('App_Filter', 'App/Filter/', 'filter');
        $this->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator/', 'decorator');
    }
	
    /**
     * 
     * Register Form
     * 
     * e-mail
     * password
     * re-password
     * nickname
     * Фамилия
     * Имя
     * Отчество
     * 
     * 
     */
    public function RegisterForm()
    {
    	// Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction('');
        
        // Указываем метод формы
        $this->setMethod('post');
        
        // Задаем атрибут class для формы
        $this->setAttrib('class', 'profile');
        
        
        // E-Mail
        $email = new App_Form_Element_Email('email', array(
            'required'    => true,
        ));
        
        $email->setAttrib('class', 'input_text');
        
        // Password
        $password = new Zend_Form_Element_Password('password', array(
        	'required'		=> true,
        	'label'			=> 'Пароль:',
        	'maxlength'		=> '100',
        	'validators'	=> array(
        		array('StringLength', true, array(3, 100))
        		),
        		
        		));
        $password->setAttrib('class', 'input_text');
       
		// ReType Password
		 $re_password = new Zend_Form_Element_Password('re_password', array(
        	'required'		=> true,
        	'label'			=> 'Пароль еще раз:',
        	'maxlength'		=> '100',
        	'validators'	=> array(
        		array('StringLength', true, array(3, 100)),
        		array('EqualInputs', true, array('password'))
        		),
        		
		));
		$re_password->setAttrib('class', 'input_text');
        		
		// nickname
		$nickname = new Zend_Form_Element_Text('nickname', array ( 
			'required'		=> true,
			'label'			=> 'Псевдоним:',
			'maxlength'		=> '100',
			'validators'	=> array(
				array('StringLength', true, array(3, 100))
			),
		
		
		));
		$nickname->setAttrib('class', 'input_text');
		
		//surename
		$surename = new Zend_Form_Element_Text('surename', array (
			'required'		=> true,
			'label'			=> 'Фамилия:',
			'maxlength'		=> '100',
			'validators'	=>	array (
				array('StringLength', true, array(2, 100)),
				array('Alnum', true, array(true))
			),
				
		));
		$surename->setAttrib('class', 'input_text');
		
		//firstname
		$firstname = new Zend_Form_Element_Text('firstname', array (
			'required'		=> true,
			'label'			=> 'Имя:',
			'maxlength'		=> '100',
			'validators'	=>	array (
				array('StringLength', true, array(2, 100)),
				array('Alnum', true, array(true))
			),
				
		));
		$firstname->setAttrib('class', 'input_text');
		
		//Отчество
		$patronymic = new Zend_Form_Element_Text('patronymic', array (
			'required'		=> true,
			'label'			=> 'Отчество:',
			'maxlength'		=> '100',
			'validators'	=>	array (
				array('StringLength', true, array(2, 100)),
				array('Alnum', true, array(true))
			),
				
		));
		$patronymic->setAttrib('class', 'input_text');
		
		$config = new Zend_Config_Ini('../../strikepro.ru/application/configs/application.ini', 'production');
		
		$captcha = new Zend_Form_Element_Captcha('captcha',
         			array(	'label' => 'Введите символы с картинки:',  
         					'captcha' => array(
					         'captcha' => 'Image',  
					         'wordLen' => 4,  
					         'timeout' => 300,  
					         'font' => APPLICATION_PATH . '/configs/Pragmatica_Regular.Ttf',  
					         'imgDir' => $config->basepath .'/images/captchas/',   
					         'imgUrl' => 'https://shop.strikepro.ru/images/captchas/',
 						)));
 		$captcha->setAttrib('class', 'input_text');
		
		// Субмит
		$submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
		));
		
		
		$this->addElements(array($email, $password, $re_password, $nickname, $surename, $firstname, $patronymic, $captcha, $submit));
		$this->addDisplayGroup(	array('email',  'password', 're_password', 'nickname', 'surename', 'firstname', 'patronymic', 'captcha', 'submit'), 
 								'MarketDataGroup', 
 								array('legend' => '')
							);
		
    }
    
}
