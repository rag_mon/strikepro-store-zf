/**
 * Created by Aleksandr Taran on 29.08.2016.
 */
function itemplusminus(v)
{
    var value = parseInt($('#quantity').val());
    value += v;

    if (value < 1)
    {
        value = 1;
    }
    if(value > 99)
    {
        value = 99;
    }
    $('#quantity').val(value);
}

$(function() {

    var Articles = $(document).find('.color_choice');
    var Thumbnails = $(document).find('.color_choice').parent();
    var circles = $('#shopModal').find('.ratio');

    Thumbnails.on('click', function(){
        var current_article = $(this).find('.color_choice').attr('id');

        set_current_article(current_article);
        select_current_circle(circles, current_article);
        $('#shopModal').modal();
    });

    circles.on('click', function() {
        circles.removeClass("img-circle-active");
        $(this).addClass("img-circle-active");

        set_current_article($(this).attr('data-article'));
    });

    $('#shopModal').on('hide.bs.modal', function (e) {
        circles.removeClass("img-circle-active");
    });

    function select_current_circle(circles, current_article)
    {
        var matches = jQuery.grep(circles, function(item) {
            return($("#" + item.id).attr('data-article') === current_article);
        });
        // matches contains all objects that matches
        if (matches.length) {
            $("#" + matches[0].id).addClass("img-circle-active");
        }
    }

    function set_current_article(current_article_id) {

        $('#shopModal').find('#article-art').text($(document).find('#'+ current_article_id).attr('id'));
        $('#shopModal').find('#article-code').text($(document).find('#'+ current_article_id).attr('data-code'));
        $('#shopModal').find('#article-color').text($(document).find('#'+ current_article_id).attr('data-color'));
        $('#shopModal').find('#article-price').text($(document).find('#'+ current_article_id).attr('data-price'));

        $('#shopModal').find('#article-image').attr('src', $(document).find('#'+ current_article_id).attr('src'));

        $('#loader_image').show();
        $('#article-image').hide();

        $('#article-image').on('load', function() {
            $('#loader_image').hide();
            $('#article-image').show();
        });

        if($(document).find('#'+ current_article_id).attr('data-insale') == 1) {
            $('#art').val($(document).find('#'+ current_article_id).attr('id'));
            $('#addtocartform').show();
            $('#out-of-order').hide();
        }
        else {
            $('#art').val('');
            $('#addtocartform').hide();
            $('#out-of-order').show();
        }


    }

    $('#quantity').change(function() {
        if(Math.floor($('#quantity').val()) !== $('#quantity').val() && !($.isNumeric($('#quantity').val())))
        {
            $(this).val(1);
        }

        if( $('#quantity').val() <= 0) {
            $(this).val(1);
        }
    });

    $('#addtocartform').submit(function(){
        addToCart($(this));
        return false;
    });
});