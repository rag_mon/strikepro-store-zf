/**
 * Created by wss on 31.08.2016.
 */

function create__basket() {
    $.ajax({
        type : 'GET',
        url : '/cart/getajaxproducts',
        success : function(data)
        {
            if(data.number_of_goods_in_cart != 0){
                $(".basket__count,.basket__content").show();
                $(".basket__notic").hide();
                $("#count-item").text(data.number_of_goods_in_cart);
                $(".count-item").text(data.number_of_goods_in_cart);
                $(".basket__count-totals__price").text(data.cost_of_goods);
                create_content_popup_basket(data.goods_list);
            } else {
                $(".basket__count,.basket__content").hide();
                $(".basket__notic").show();
            }
        }
    });
}


function create_content_popup_basket(array) {
    $("#js-basket__content").empty();
    var length__array = array.length;
    for(var i = 0; i < length__array; i++){
        var template = "<tr><td >"+
            array[i].goods_name
            +"</td><td>"+
            check_str(array[i].goods_price)+'<i class="fa fa-rub" aria-hidden="true"></i>'
            +"</td><td>"+
            array[i].number_of_goods
            +"</td><td>"+
            array[i].total_cost_goods+'<i class="fa fa-rub" aria-hidden="true"></i>'
            +"</td></tr>";
        $("#js-basket__content").append(template);
    }

}

function check_str(str) {
    return str.substring(0, str.indexOf(".") + 3);
}


function recount_dlvr(){

    var radio = $('#cartform input[name=dlvr_mode_idx]:checked');
    var dlvrcosts = parseFloat($(radio).parent().find('input[type=hidden]').val());
    var total = 0;
    var curr_roundlimit = 100;

    if(parseFloat($('#cartform input[name=wodeliverycosts]').val())>parseFloat($('#cartform input[name=deliveryfreeamount]').val()))
    {
        //free
        total = parseFloat($('#cartform input[name=wodeliverycosts]').val());

        $('#cart_products_delivery').text(parseFloat(0.00));
        blink('#cart_products_delivery', 3);

        $('#cart_products_summ').text(total);
        blink('#cart_products_summ', 3);
    }
    else{
        total = parseFloat($('#cartform input[name=wodeliverycosts]').val())+parseFloat(dlvrcosts);

        $('#cart_products_delivery').text(parseFloat((Math.round(dlvrcosts*curr_roundlimit))/curr_roundlimit));
        blink('#cart_products_delivery', 3);

        $('#cart_products_summ').text(parseFloat((Math.round(total*curr_roundlimit))/curr_roundlimit));
        blink('#cart_products_summ', 3);
    }
}

function upd_row_and_cart(suff_num, msg)
{
    var pidx = 0;

    for(pidx=0;pidx<msg.products.length;pidx++)
    {
        if(msg.products[pidx].product_art == $('#cartform input[name=art_row_'+(suff_num)+']').val())
        {
            $('#cartform').find('#product_row_' + suff_num).find('.art_total').text((msg.products[pidx].row_total_fmt));
            blink($('#cartform').find('#product_row_' + suff_num).find('.art_total'), 3);

            $('#cartform input[name=qty_row_'+(suff_num)+']').val(parseInt(msg.products[pidx].product_count));

            if(parseInt(msg.products[pidx].product_count)==0)
            {
                $('#product_row_'+(suff_num)).addClass('zerocount');
            }
            else {
                $('#product_row_'+(suff_num)).removeClass('zerocount');
            }

        }
    }

    $('#cartform input[name=wodeliverycosts]').val(msg.rawcart.total);

    recount_dlvr();

    $('#cartblock_countarts').text(msg.rawcart.countarts);

    $('#cartblock_countitems').text(msg.rawcart.countitems);
    blink('#cartblock_countitems', 3);

    $('#cartblock_summ').text(msg.rawcart.total_fmt);
    blink('#cartblock_summ', 3);

}


function blink(selector, repeat){
    if(!repeat) return;
    $(selector).fadeOut('fast', function(){
        $(this).fadeIn('fast', function(){
            blink(this, repeat - 1);
        });
    });
}

function pre_update_row(qty_input)
{

    var suff_num = qty_input.first().attr('name');

    suff_num = parseInt(suff_num.replace(/^.+?(\d+)$/, '$1'));

    var product_qty = qty_input.first().val();
    var product_art = qty_input.parent().prev().val();

    var msg = setProdQty(product_art,product_qty);

    if(msg){
        upd_row_and_cart(suff_num, msg);
        create__basket();
    }
    else{
        $.jGrowl('Ошибка AJAX запроса', {
            speed:  'slow'
        });
    }
    return false;
}

$(document).ready(function(){

    $('#cartform input.artqtyinput').keypress(function(e) {
        if (e.keyCode == '13')
        {
            e.preventDefault();
            var suff_num = $(e.target).first().attr('name');

            suff_num = parseInt(suff_num.replace(/^.+?(\d+)$/, '$1'));

            var product_qty = $(e.target).first().val();
            var product_art = $(e.target).first().prev('input').val();

            var msg = setProdQty(product_art, product_qty);

            if(msg)
            {
                upd_row_and_cart(suff_num, msg);
            }
            else {
                $.jGrowl('Ошибка AJAX запроса', {
                    speed:  'slow'
                });
            }

            return false;
        }
        else {
            if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
            {
                e.preventDefault();
                $.jGrowl('Поле допускает ввод только цифр', {
                    speed:  'slow'
                });
                return false;
            }
            else {
                return true;
            }
        }
    });

    $('#cartform input.artqtyinput').bind("change", function() {
        pre_update_row($(this));
    });

    $(document).find('.quantity_minus').click(function() {

        var qty_input = $(this).next();

        if (qty_input.val() > 1)
        {
            qty_input.val(parseInt(qty_input.val()) - 1);
            pre_update_row(qty_input);
        }
        else {
            return true;
        }


    });

    $(document).find('.quantity_plus').click(function() {
        var qty_input = $(this).prev();
        qty_input.val(parseInt(qty_input.val()) + 1);
        pre_update_row(qty_input);
    });


    // // basket tatal always visible
    // var top = $('.preorder_list').offset().top -270;
    //
    // $(document).scroll(function(){
    //     $('.preorder_list').css('position','');
    //     top = $('.preorder_list').offset().top - 270;
    //     $('.preorder_list').css('position','absolute');
    //     $('.preorder_list').css('top', Math.max(top,$(document).scrollTop()));
    // });

});
