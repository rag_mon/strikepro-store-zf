<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 21.08.2016
 * Time: 17:04
 */
require_once 'models/ObjectPhotoModel.php';

class ObjectPhoto
{
    protected $_model;
    protected $_table = 'catalog_photo';

    private $property = array(
        'Id',
        'cat_code',
        'primary',
        'file',
        'alt'
    );

    public function __construct($item)
    {
        if (!empty($item)) {
            foreach ($item as $property => $argument) {
                if(in_array($property, $this->property)) {
                    $this->{$property} = $argument;
                }
            }
        }
    }

    public static function delete($Id)
    {
        return Application_Model_ObjectPhoto::getDefaultAdapter()->delete('catalog_photo', array('Id = ?' => $Id));
    }

    public static function get($Id)
    {
        return Application_Model_ObjectPhoto::getDefaultAdapter()->fetchRow(
            Application_Model_ObjectPhoto::getDefaultAdapter()->select()->from('catalog_photo')->where('Id = ?', $Id));
    }

    public static function update($Id, $data)
    {
        return Application_Model_ObjectPhoto::getDefaultAdapter()->update('catalog_photo', $data, array('Id = ?' => $Id));
    }

    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value) {
            $array[$value] = $this->{$value};
        }
        return $array;
    }
}