<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 24.08.2016
 * Time: 12:56
 */

require_once 'entity/CatalogObject/AbstractCatalogObjectFactory.php';

class STPR_View_Helper_GetObjectPhoto extends Zend_View_Helper_Abstract
{
    private $output = null;

    public function GetObjectPhoto($code)
    {
        $this->catalog_object
            = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($code));

        foreach($this->catalog_object->photo AS $photo) {
            $this->output .=
            "<div id=\"photo_id_$photo->Id\" class=\"col-sm-6 col-md-4\">
                <div class=\"thumbnail\">
                    <div class=\"dropdown\" style=\"position:absolute; right:30px; top:10px;\">";

                        if(!is_null($photo->primary)) {
                            $this->output .= "<span class=\"btn disable\" title=\"Основная\"><i style=\"color:rgb(123, 181, 48)\" class=\"fa fa-check-square fa-2x\" aria-hidden=\"true\"></i></span>";
                        }

            $this->output .= "<button class=\"btn dropdown-toggle\" type=\"button\" id=\"photoAction\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">
                            <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>
                            </button>
                   
                        <ul class=\"dropdown-menu\" aria-labelledby=\"photoAction\">
                            <li><a class=\"primary-photo-button\"href=\"#\" >Назначить основной</a></li>
                            <li><a class=\"edit-photo-button\" data-id=\"$photo->Id\" data-toggle=\"modal\" data-target=\"#editPhotoModal\" href=\"#\">Редактировать описание</a></li>
                            <li><a class=\"delete-photo-button\" href=\"#\" data-id=\"$photo->Id\" data-toggle=\"modal\" data-target=\"#deletePhotoModal\" >Удалить</a></li>
                        </ul>
                    </div>
                    
                    <img style=\"max-height:180px;\" class=\"img-responsive\" src=\"$photo->file\" alt=\"$photo->alt\">
                    <div class=\"caption\">
                        <p>$photo->alt</p>
                    </div>
                </div>
            </div>\n";
        }
        return $this->output;
    }
}



