<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 20.08.2016
 * Time: 17:48
 */

require_once 'entity/CatalogObject/CatalogObject.php';
require_once 'entity/Features/FeaturesFactory.php';
require_once 'entity/ObjectImages/ObjectImagesFactory.php';
require_once 'entity/ObjectPhoto/ObjectPhotoFactory.php';
require_once 'entity/Articles/ArticlesFactory.php';

class CatalogObjectProductDirectory extends CatalogObject implements CatalogObjectInterface
{
    /**
     * @var array of object property
     */
    private $property = array(  'cat_code',
                                'cat_object_type',
                                'cat_sub_of', 
                                'cat_position', 
                                'cat_refactor', 
                                'cat_name', 
                                'cat_link',
                                'cat_image',
                                'cat_image_small',
                                'cat_description',
                                'cat_show_on_site',
                                'cat_codename',
                                'cat_hook_type',
                                'cat_hook_name',
                                'cat_weight',
                                'cat_length',
                                'cat_tension',
                                'cat_depth_min',
                                'cat_depth_max',
                                'cat_type',
                                'cat_type1',
                                'cat_video',
                                'cat_video_howto',
                                'cat_360_pattern',
                                'cat_new',
                                'cat_fullname'
                            );
    /**
     * @var array of Feature Object
     */
    public $features;

    /**
     * @var array of ObjectImages
     */
    public $images;
    public $photo;
    public $articles;

    private static $instance;

    public static function getInstance($Id, $id_type = null)
    {
        self::$instance = new self($Id, $id_type);
        return self::$instance;
    }

    /**
     * __constructor is closed
     */
    private function __construct($Id, $id_type)
    {
        $this->_model = new Application_Model_CatalogObject();

        if ($id_type == 'link') {
            $DbObject = $this->getCatalogObjectByLink($Id);
        }
        else {
            $DbObject = $this->getCatalogObject($Id);
        }

        if (!empty($DbObject)) {
            foreach ($DbObject as $property => $argument) {
                if(in_array($property, $this->property))
                    $this->{$property} = $argument;
            }
        }
        if($this->cat_refactor == 1) {
            $this->setPattern('new-wobbler');
        }
        else {
            switch ($this->cat_object_type) {
                case 'Рыболовный товар': $this->setPattern('accessories'); break;
                case 'Фурнитура': $this->setPattern('accessories'); break;
                case 'Воблер': $this->setPattern('old-wobbler'); break;
                case 'Блесна-Цикада': $this->setPattern('old-spoon'); break;
                case 'Аксессуар': $this->setPattern('accessories'); break;
                case 'БассБейт': $this->setPattern('old-spoon'); break;
                case 'Блесна': $this->setPattern('old-spoon'); break;
                case 'Вращающаяся блесна': $this->setPattern('old-spoon'); break;
                case 'Леска': $this->setPattern('old-line'); break;
                case 'Балансир': $this->setPattern('old-icebalance'); break;
                case 'Мягкая приманка': $this->setPattern('old-softbaits'); break;
                case 'Спиннинг': $this->setPattern('old-spinning'); break;
                case 'Спиннербейт': $this->setPattern('old-spoon'); break;
                case 'Плетеный шнур': $this->setPattern('old-braid'); break;
                case 'Тейл спиннер': $this->setPattern('old-spoon'); break;
                case 'Поводок': $this->setPattern('accessories'); break;
                case 'Головка джигерная': $this->setPattern('accessories'); break;
                case 'Крючки': $this->setPattern('accessories'); break;
                case 'разборная чебурашка': $this->setPattern('accessories'); break;
                case 'Шнур рыболовный': $this->setPattern('old-braid'); break;
                default: $this->setPattern('old-wobbler'); break;
            }
        }


//        $this->features = FeaturesFactory::getInstance($this->cat_code)->get();
//        $this->images = ObjectImagesFactory::getInstance($this->cat_code)->get();
//        $this->photo = ObjectPhotoFactory::getInstance($this->cat_code)->get();
//        $this->articles = GoodsFactory::getInstance($this->cat_code)->get();
    }

    public function save()
    {
        // self
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }
        $this->_model->save($array);


        //feature
        foreach($this->features AS $feature)
        {
            $feature->save();
        }

        return true;
    }

    public function addImage($file, $alt = null, $primary = null)
    {
        // cat_code, type, primary, file, alt.
        $data['cat_code'] = $this->cat_code;
        $data['file'] = $file;
        $data['primary'] = $primary;
        $data['alt'] = $alt;

        $this->images = ObjectImagesFactory::getInstance($this->cat_code)->addImage($data)->getObjectImages();
    }

    public function addPhoto($file, $alt = null, $primary = null)
    {
        // cat_code, type, primary, file, alt.
        $data['cat_code'] = $this->cat_code;
        $data['file'] = $file;
        $data['primary'] = $primary;
        $data['alt'] = $alt;

        $this->photo = ObjectPhotoFactory::getInstance($this->cat_code)->addPhoto($data)->getObjectPhoto();
    }

    /**
     * deny __clone function
     */
    private function __clone()
    {

    }

    /**
     * deny serialization
     */
    private function __sleep()
    {

    }

    /**
     * deny deserialization
     */
    private function __wakeup()
    {

    }

    /**
     *
     * print_r or var_dump only allowed property
     * @return mixed
     */
    public function __debugInfo()
    {
        foreach ($this->property AS $key => $value)
        {
            $array[$value] = $this->{$value};
        }

        $array['features'] = $this->features;
        $array['images'] = $this->images;
        $array['photo'] = $this->photo;
        $array['articles'] = $this->articles;

        return $array;
    }
}


