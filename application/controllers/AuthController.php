<?php

/**
 * AuthController
 * 
 * @author
 * @version 
 */

require_once 'App/Controller/Action.php';

class AuthController extends App_Controller_Action
{

    public function init()
    {
        parent::init();
        /* Initialize action controller here */
    	$this->view->title = "аунтентификация";
    	
    }

    public function indexAction()
    {
        $this->_helper->redirector('login','auth');
    }
    
	public function loginAction()
	{
		/* если пользователь уже зашел, отправляем его на главную */
		if (Zend_Auth::getInstance()->hasIdentity())
		{
			$this->_helper->redirector('index', 'index');
		}
		
		/* Form_Login - это наследник Zend_From, который создает форму для авторизации */
		$loginForm = new Application_Form_Login();
	 
		/* Проверяем,если были отправлены POST данные */
		if( $this->_request->isPost() )
		{
			/* Проверяем валидность данных формы */
			if ( $loginForm->isValid($this->_getAllParams()) )
			{

				
				/* Создаем адаптер в виде базы данных */
	            $authAdapter = new Zend_Auth_Adapter_DbTable();
				
				/** 
				 * Настраиваем правила выборки пользователей из БД 
				 * Соответственно, имя таблицы, 
				 * название поля с идентификатором пользователя,
				 * название поля для сверки "пароля"
				 */
             	$authAdapter->setTableName('users')
							->setIdentityColumn('email')
             				->setCredentialColumn('password');

				/* Передаем в адаптер данные пользователя.*/
             	$authAdapter->setIdentity($loginForm->getValue('email'));
                $authAdapter->setCredential(SHA1($loginForm->getValue('password')));
            	
            	/* Собственно, процесс аутентификации */
            	$auth = Zend_Auth::getInstance();
            	$resultAuth = $auth->authenticate($authAdapter);

				/* Проверяем валидность результата */
	            if( $resultAuth->isValid() )
	            {
					/* Пишем в сессию необходимые нам данные (пароль обнуляем, он нам в сессии не нужен :) */
            		$data = $authAdapter->getResultRowObject(null, 'password');
            		$auth->getStorage()->write($data);
 
	            	/* Редиректим на страницу админа */
            		$this->_helper->redirector('index','admin');
				}
			}
	 	}
	
		/* Если данные не передавались, то Выводим пользователю форму для авторизации */
		$this->view->formLogin = $loginForm;
		$loginForm->reset();
	}
	 
	/**
	 * "Выход" пользователя
	 */
	
	public function logoutAction()
	{
	    /* "Очищаем" данные об идентификации пользоваля */
		Zend_Auth::getInstance()->clearIdentity();
		
		/** 
		 * Перебрасываем его на главную 
		 * Желательно еще как-то оповестить пользователя о том, что он вышел
		 */
	    $this->_helper->redirector('index');
	}
	
}

