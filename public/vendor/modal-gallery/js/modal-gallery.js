/**
 * Created by Aleksandr Taran on 12.08.2016.
 */
function itemplusminus(v)
{
    var value = parseInt($('#quantity').val());
    value += v;

    if (value < 1)
    {
        value = 1;
    }
    if(value > 99)
    {
        value = 99;
    }
    $('#quantity').val(value);
}

$('#quantity').change(function() {
    if(Math.floor($('#quantity').val()) !== $('#quantity').val() && !($.isNumeric($('#quantity').val())))
    {
        $(this).val(1);
    }

    if( $('#quantity').val() <= 0) {
        $(this).val(1);
    }
});

$(document).ready(function()
{
    var ImgColorChoices = $(document).find(".color_choice");

    //MODAL BEGIN
    $(ImgColorChoices).parent().click(function(){

        $('#product-image').attr('src', $(this).children('img').attr('data-big-image'));

        $('#product-article').text($(this).children('img').attr('data-article'));
        $('#product-code').text($(this).children('img').attr('data-code'));
        $('#product-color').text($(this).children('img').attr('data-color'));
        $('#product-price').text(Math.round($(this).children('img').attr('data-price')));

        $('#art').val($(this).children('img').attr('data-article'));

        if($(this).children('img').attr('data-insale') == 1 ) {
            $('#addtocartform').show();
            $('#out-of-order').hide();
        }
        else {
            $('#addtocartform').hide();
            $('#out-of-order').show();
        }

        $('#product-image').hide();
        $('#loader_image').show();

        $('#MyModal').modal();

        $('#product-image').on('load', function() {
            $('#loader_image').hide();
            $('#product-image').show();
        });
    });

    // On click in Modal color choice show it on #current_color
    $('.img-circle').click(function() {

        $('#product-image').hide();
        $('#loader_image').show();

        $('#product-image').attr('src', $(this).attr('data-big-image'));

        $('#product-article').text($(this).attr('data-article'));
        $('#product-code').text($(this).attr('data-code'));
        $('#product-color').text($(this).attr('data-color'));
        $('#product-price').text(Math.round($(this).attr('data-price')));
        $('#art').val($(this).attr('data-article'));

        $('#product-image').on('load', function(){
            $('#loader_image').hide();
            $('#product-image').delay("slow").fadeIn();
        });

        $('.img-circle').removeClass("img-circle-active");
        $(this).addClass("img-circle-active");

        if($(this).attr('data-insale') == 1 ) {
            $('#addtocartform').show();
            $('#out-of-order').hide();
        }
        else {
            $('#addtocartform').hide();
            $('#out-of-order').show();
        }
    });
    //MODAL END

    $('#addtocartform').submit(function(){
        addToCart($(this));
        return false;
    });
});