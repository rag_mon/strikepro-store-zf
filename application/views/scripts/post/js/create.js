$(document).ready(function(){
    $(function () {
        $('#datetimepicker1').datetimepicker({
            locale: 'ru',
            format: 'YYYY-MM-DD HH:00:00',
            useCurrent: true,
        });
    });

    $(function () {
        CKEDITOR.replace('textareaBody', {
        });
    });

    $(function () {
        $('#form-box').fadeIn('slow', function () {
            $('#form-box').animate({'opacity': 'show', 'paddingTop': 0}, 5000);
        });
    });
});

$('#inputTitle').on('change paste keyup', function() {
    $('#inputLink').val(translit($('#inputTitle').val()));
});

$('#chooseHeadImageButton').on('click', function() {
    var Filemanager = window.open("<?php echo $this->baseUrl('files/browse?element'); ?>=" + 'hiddenHeadImage',
        'filemanager',
        'width=1280,height=800,menubar=no,toolbar=no,location=no,status=no,resizable=no,scrollbars=yes');
});

$('#HeadImageBox').on('mouseover', function() {
    document.getElementById('overlay').style.display = 'flex';
});

$('#HeadImageBox').on('mouseout', function() {
    $('#overlay').hide();
});

$('#unsetHeadImageButton').on('click', function() {
    document.getElementById('HeadImageBox').style.display = 'none';
    document.getElementById('dropbox').style.display = 'flex';
});

var callbackFileManager = function callbackFileManager(element, value) {
    $('#' + element).val(value);
    $('#dropbox').hide();
    var HeadImg = document.getElementById("HeadImage");
    HeadImg.setAttribute("src", value);
    document.getElementById('HeadImageBox').style.display = 'flex';
};