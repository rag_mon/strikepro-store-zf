<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 17.08.2016
 * Time: 12:33
 */

require_once 'App/Controller/Admin.php';
require_once 'entity/CatalogObject/AbstractCatalogObjectFactory.php';

class ProductController extends App_Controller_Admin
{
    protected $catalog_object;

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        if (!is_null($this->_getParam('code')) && intval($this->_getParam('code'))) {

            if (!is_object($this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_getParam('code')))->withChilds())) {
                $this->_helper->getHelper('FlashMessenger')->addMessage('Запрашиваемого каталога не обнаружено!');
                $this->_helper->redirector('index', 'product');
            }
        }
        else {

            $this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(678)->withChilds();
        }

        if ($this->catalog_object->cat_object_type == 4) {

            $this->_helper->viewRenderer('product-group');
        }

        $config = new Zend_Config_Ini('configs/application.ini', 'production');
        $this->view->config = $config->products;
        $this->view->catalog_object = $this->catalog_object;

    }

    public function updateFeaturesAction()
    {
        if (!$this->_request->getParam('cat_code')) {
            $this->_helper->getHelper('FlashMessenger')->addMessage('Жулик!');
            $this->_helper->getHelper('Redirector')->gotoUrl('/product');
        }

        $this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_request->getParam('cat_code')));
        $post = $this->_request->getParams();

        //set new product features
        foreach ($post AS $key => $value) {

            foreach ($this->catalog_object->features AS $item) {

                if ($item->features_id == $key) {
                    $item->value = $value;
                }
            }
        }

        // save changes
        $this->catalog_object->save();

        // add notify message and redirect
        $this->_helper->getHelper('FlashMessenger')->addMessage('Обновление характеристик прошло успешно!');
        $this->_helper->getHelper('Redirector')->gotoUrl('/product/' . $this->catalog_object->cat_code);

        return true;
    }

    public function updateDescriptionAction()
    {
        $this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_request->getParam('cat_code')));
        $this->catalog_object->cat_description = htmlspecialchars(trim($this->getRequest()->getParam('description')));

        $this->catalog_object->save();

        $this->_helper->getHelper('FlashMessenger')->addMessage('Обновление описания прошло успешно!');
        $this->_helper->getHelper('Redirector')->gotoUrl('/product/' . $this->catalog_object->cat_code);
    }

    public function uploadImageAction()
    {
        if($this->getRequest()->isXmlHttpRequest()) {

            $this->catalog_object
                = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_request->getParam('code')));

            if(!$this->catalog_object->cat_codename) exit($this->_helper->json(array('success' => false)));

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $config = new Zend_Config_Ini('configs/application.ini', 'production');

            $upload_dir = $this->DirectoryCheckForExist($config->basepath
                            . $config->products->directory->base
                            . '/' . $this->catalog_object->cat_codename
                            . $config->products->directory->image);



            require_once 'plugins/simple-ajax-loader/Uploader.php';
            $Upload = new FileUpload('uploadfile');
            $Upload->newFileName = $this->catalog_object->cat_link ."_".$this->catalog_object->cat_codename ."_".uniqid(mt_rand(), true).".".$Upload->getExtension();
            $result = $Upload->handleUpload($upload_dir, array('png', 'jpeg', 'jpg'));

            if (!$result) {
                $this->_helper->json(array('success' => false,
                                            'msg' => $Upload->getErrorMsg()
                ));
            }
            else {
                $file = $config->products->directory->base
                            . '/' . $this->catalog_object->cat_codename
                            . $config->products->directory->image
                            . '/' . $Upload->getFileName();

                $alt = $this->catalog_object->cat_name . ' (' . $this->catalog_object->cat_code . '), вид:';

                $this->catalog_object->addImage($file, $alt, null);

                require_once APPLICATION_PATH . '/views/helpers/getObjectImage.php';
                $helper = new STPR_View_Helper_getObjectImage();

                $this->_helper->json(array('success' => true,
                                            'msg' => 'файл загружен',
                                            'output' => $helper->getObjectImage($this->catalog_object->cat_code)
                ));
            }
            return true;
        }
        throw new Zend_Http_Exception('not allowed', 503);
    }

    public function uploadProgressAction()
    {
        if($this->getRequest()->isXmlHttpRequest()) {

            if (isset($_SERVER['HTTP_ORIGIN'])) {

                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }

            if (isset($_REQUEST['progresskey'])) {
                $status = apc_fetch('upload_'.$_REQUEST['progresskey']);
            }
            else {
                exit($this->_helper->json(array('success' => false)));
            }

            $pct = 0;
            $size = 0;

            if (is_array($status)) {
                if (array_key_exists('total', $status) && array_key_exists('current', $status)) {

                    if ($status['total'] > 0) {

                        $pct = round(($status['current'] / $status['total']) * 100);
                        $size = round($status['total'] / 1024);
                    }
                }
            }

            $this->_helper->json(
                array(  'success' => true,
                        'pct' => $pct,
                        'size' => $size
                ));
        }
    }

    public function deleteImageAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            require_once 'entity/ObjectImages/ObjectImage.php';

            $config = new Zend_Config_Ini('configs/application.ini', 'production');
            $ObjectImage = ObjectImage::get($this->_request->getParam('Id'));

            if($ObjectImage) {
                try {
                    unlink($config->basepath.$ObjectImage['file']);

                    $delete_result = ObjectImage::delete($this->_request->getParam('Id'));

                    $this->_helper->json(
                        array( 'msg' => $delete_result,
                            'Ident' => $this->_request->getParam('Id'))
                    );
                }
                catch (Zend_Exception $e)
                {
                    throw new Zend_Exception($e);
                }
            }
            else {
                $this->_helper->json(
                    array('msg' => 0,
                        'Ident' => $this->_request->getParam('Id'))
                );
            }
        }
        else {
            die('access denied');
        }
    }

    public function imageInfoAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            require_once 'entity/ObjectImages/ObjectImage.php';
            $ObjectImage = ObjectImage::get(intval($this->_request->getParam('Id')));

            if($ObjectImage) {
                $this->_helper->json(
                    array( 'msg' => 1, 'alt' => $ObjectImage['alt']));
            }
            else {
                $this->_helper->json(array('msg' => 0));
            }
        }
        else {
            die('access denied');
        }
    }

    public function editImageAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            require_once 'entity/ObjectImages/ObjectImage.php';
            $ObjectImage = ObjectImage::get(intval($this->_request->getParam('Id')));

            if($ObjectImage) {
                try {
                    $data = array('alt'=> htmlentities($this->_request->getParam('alt')));
                    ObjectImage::update(intval($this->_request->getParam('Id')), $data);

                    $this->catalog_object
                        = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_request->getParam('code')));

                    if(!$this->catalog_object->cat_code) {
                        $this->_helper->json(
                            array('msg' => 'object not found')
                        );

                        return true;
                    }

                    require_once APPLICATION_PATH . '/views/helpers/getObjectImage.php';
                    $helper = new STPR_View_Helper_getObjectImage();

                    $this->_helper->json(
                        array('msg' => 1,
                            'output' => $helper->getObjectImage($this->catalog_object->cat_code)
                            )
                    );
                }
                catch (Zend_Exception $e)
                {
                    throw new Zend_Exception($e);
                }
            }
            else {
                $this->_helper->json(
                    array('msg' => 'object not found')
                );

                return true;
            }

        }
        else {
            die('access denied');
        }
    }

    public function deletePhotoAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            require_once 'entity/ObjectPhoto/ObjectPhoto.php';

            $config = new Zend_Config_Ini('configs/application.ini', 'production');
            $ObjectPhoto = ObjectPhoto::get($this->_request->getParam('Id'));

            if($ObjectPhoto) {
                try {
                    unlink($config->basepath.$ObjectPhoto['file']);

                    $delete_result = ObjectPhoto::delete($this->_request->getParam('Id'));

                    $this->_helper->json(
                        array( 'msg' => $delete_result,
                            'Ident' => $this->_request->getParam('Id'))
                    );
                }
                catch (Zend_Exception $e)
                {
                    throw new Zend_Exception($e);
                }
            }
            else {
                $this->_helper->json(
                    array('msg' => 0,
                        'Ident' => $this->_request->getParam('Id'))
                );
            }
        }
        else {
            die('access denied');
        }
    }

    public function editPhotoAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            require_once 'entity/ObjectPhoto/ObjectPhoto.php';
            $ObjectPhoto = ObjectPhoto::get(intval($this->_request->getParam('Id')));

            if($ObjectPhoto) {
                try {
                    $data = array('alt'=> htmlentities($this->_request->getParam('alt')));
                    ObjectPhoto::update(intval($this->_request->getParam('Id')), $data);

                    $this->catalog_object
                        = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_request->getParam('code')));

                    if(!$this->catalog_object->cat_code) {
                        $this->_helper->json(
                            array('msg' => 'object not found')
                        );

                        return true;
                    }

                    require_once APPLICATION_PATH . '/views/helpers/getObjectPhoto.php';
                    $helper = new STPR_View_Helper_getObjectPhoto();

                    $this->_helper->json(
                        array('msg' => 1,
                            'output' => $helper->getObjectPhoto($this->catalog_object->cat_code)
                        )
                    );
                }
                catch (Zend_Exception $e)
                {
                    throw new Zend_Exception($e);
                }
            }
            else {
                $this->_helper->json(
                    array('msg' => 'object not found')
                );

                return true;
            }

        }
        else {
            die('access denied');
        }
    }

    public function photoInfoAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            require_once 'entity/ObjectPhoto/ObjectPhoto.php';
            $ObjectPhoto = ObjectPhoto::get(intval($this->_request->getParam('Id')));

            if($ObjectPhoto) {
                $this->_helper->json(
                    array( 'msg' => 1, 'alt' => $ObjectPhoto['alt']));
            }
            else {
                $this->_helper->json(array('msg' => 0));
            }
        }
        else {
            die('access denied');
        }
    }

    public function uploadPhotoAction()
    {
        if($this->getRequest()->isXmlHttpRequest()) {

            $this->catalog_object
                = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(intval($this->_request->getParam('code')));

            if(!$this->catalog_object->cat_codename) exit($this->_helper->json(array('success' => false)));

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $config = new Zend_Config_Ini('configs/application.ini', 'production');

            $upload_dir = $this->DirectoryCheckForExist($config->basepath
                . $config->products->directory->base
                . '/' . $this->catalog_object->cat_codename
                . $config->products->directory->photo);



            require_once 'plugins/simple-ajax-loader/Uploader.php';
            $Upload = new FileUpload('uploadfile-photo');
            $Upload->newFileName = $this->catalog_object->cat_link ."_".$this->catalog_object->cat_codename ."_".uniqid(mt_rand(), true).".".$Upload->getExtension();
            $result = $Upload->handleUpload($upload_dir, array('png', 'jpeg', 'jpg'));

            if (!$result) {
                $this->_helper->json(array('success' => false,
                    'msg' => $Upload->getErrorMsg()
                ));
            }
            else {
                $file = $config->products->directory->base
                    . '/' . $this->catalog_object->cat_codename
                    . $config->products->directory->photo
                    . '/' . $Upload->getFileName();

                $alt = $this->catalog_object->cat_name . ' (' . $this->catalog_object->cat_codename . '), фотография:';

                $this->catalog_object->addPhoto($file, $alt, null);

                require_once APPLICATION_PATH . '/views/helpers/getObjectPhoto.php';
                $helper = new STPR_View_Helper_getObjectPhoto();

                $this->_helper->json(array('success' => true,
                    'msg' => 'файл загружен',
                    'output' => $helper->getObjectPhoto($this->catalog_object->cat_code)
                ));
            }
            return true;
        }
        throw new Zend_Http_Exception('not allowed', 503);
    }



    private function DirectoryCheckForExist($directory)
    {
        if (!file_exists($directory)) {

            try {
                mkdir($directory, 0777, true);
            }
            catch (Zend_Exception $e) {
                throw new Zend_Exception('I can\'t create directory', 404);
            }
        }

        return $directory;
    }


    /**
     * TEST ACTION
     */
    public function testAction()
    {
        $this->catalog_object = AbstractCatalogObjectFactory::getCatalogObjectFactory()->getCatalogObject(1083);
        echo "<pre>"; print_r($this->catalog_object); echo "</pre>"; exit;
        return true;
    }

    public function extremeAction()
    {
        $this->_model = new Application_Model_CatalogObject();
        $this->_model->insertAllFeatures();
    }

}
