<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 28.08.2016
 * Time: 17:19
 */

require_once 'entity/Goods/Goods.php';
require_once 'models/GoodsModel.php';


class GoodsFactory
{
    protected $_model;
    protected $goods;
    private static $instance;

    private function __construct($id)
    {
        $this->_model = new Application_Model_Goods();

        $this->goods = new Goods($this->_model->getGoods($id));
    }

    public static function getInstance($id)
    {
        self::$instance = new self($id);
        return self::$instance;
    }

    public function get()
    {
        return $this->goods;
    }
}