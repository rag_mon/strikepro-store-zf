<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 21.08.2016
 * Time: 17:04
 */
require_once 'entity/Features/Feature.php';
require_once 'models/FeaturesModel.php';


class FeaturesFactory
{
    protected $_model;
    protected $features = array();
    private static $instance;

    private function __construct($Id)
    {
        $this->_model = new Application_Model_Features();

        foreach ($this->_model->getProductFeatures($Id) AS $item){

            $this->pushFeature(new Feature($item));
        }
    }

    private function pushFeature(Feature $feature)
    {
        $this->features[] = $feature;
    }


    public static function getInstance($Id)
    {
        self::$instance = new self($Id);
        return self::$instance;
    }

    public function get()
    {
        return $this->features;
    }
}