<?php

/**
 * @see Zend_Filter_Interface
 */
require_once 'Zend/Filter/Interface.php';

/**
 * Переименовывает файл присваивая ему уникальное имя
 */
class App_Filter_File_SetUniqueName implements Zend_Filter_Interface
{
    /**
     * @var array Настройки фильтра
     */
    private $_options;

    public function __construct($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } elseif (is_string($options)) {
            $options = array(
                'targetDir' => $options,
                'nameLength' => 36
            );
        } elseif (!is_array($options)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception('Invalid options argument provided to filter');
        }
        
        $this->_options = $options;
    }

    /**
     * Собственно сам фильтр
     * 
     * @param  string $fileSource Полный путь к исходному файлу
     * @return string Путь к новому файлу или exception в случае неудачи
     */
    public function filter($fileSource)
    {
        if (! file_exists($fileSource)) {
            return $fileSource;
        }

        $pathInfo = pathinfo($fileSource);
        $targetDir = isset($this->_options['targetDir']) ? $this->_options['targetDir'] : $pathInfo['dirname'];

        // Пока не будет сгенерировано уникальное имя
        while(true) {
            $fileName = self::_randStr($this->_options['nameLength']) . '.' . $pathInfo['extension'];
            $fileTarget = $targetDir . DIRECTORY_SEPARATOR . $fileName;
            if (! file_exists($fileTarget)) {
                break;
            }
        }

        $result = rename($fileSource, $fileTarget);

        if ($result === true) {
            return $fileTarget;
        }
        
        require_once 'Zend/Filter/Exception.php';
        throw new Zend_Filter_Exception(sprintf("File '%s' could not be renamed. An error occured while processing the file.", $fileSource));
    }


    /**
     * Генерирует случайную строку указанной длины
     * 
     * @param integer $length
     * @return string
     */
    private static function _randStr($length = 10)
    {
        $chars = array('a','b','c','d','e','f',
                        'g','h','i','j','k','l',
                        'm','n','o','p','r','s',
                        't','u','v','x','y','z',
                        'A','B','C','D','E','F',
                        'G','H','I','J','K','L',
                        'M','N','O','P','R','S',
                        'T','U','V','X','Y','Z',
                        '1','2','3','4','5','6',
                        '7','8','9','0');

        $name = "";

        for($i = 0; $i < $length; $i++) {
            $index = rand(0, count($chars) - 1);
            $name .= $chars[$index];
        }

        return $name;
    }
}