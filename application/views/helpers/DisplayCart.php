<?php

class STPR_View_Helper_DisplayCart extends Zend_View_Helper_Abstract {
      
    protected $_last_error = false;
    protected $_cartModel = false;

    public function DisplayCart($mode = false, $dlvr_mode_idx = false)
    {
        $delivery_cfg = new Zend_Config_Ini('../application/configs/strikepro.ini', 'delivery');
        $dlv_idx = $deliverycosts = 0;

        if(!intval($dlvr_mode_idx) && array_key_exists('dlvr_mode_idx', $_REQUEST))
        {
            $dlvr_mode_idx = intval($_REQUEST['dlvr_mode_idx']);
        }

        if(!intval($dlvr_mode_idx))
        {
            $dlvr_mode_idx = 1;
        }

        if( intval($dlvr_mode_idx) > count($delivery_cfg->deliverynames) )
        {
            $dlvr_mode_idx = 1;
        }

        foreach( $delivery_cfg->deliverynames as $dlv_name )
        {
            $dlv_idx++;

            if($dlvr_mode_idx == $dlv_idx)
            {
                $deliverycosts = $delivery_cfg->deliveryprices->$dlv_idx;
            }
        }


        $cart_html = '<!-- application/views/helpers/DisplayCart.php -->';

        if(!$this->_cartModel)
        {
            $this->_cartModel = new Application_Model_CartModel();
        }

        $cart_products = $this->_cartModel->getCartProducts();

        $cart_total = 0;
        $cart_count = 0;

        foreach ($cart_products as $productrow)
        {
            $cart_count += $productrow['product_count'];
            $cart_total += round($productrow['product_count'] * round($productrow['art_price'], 2), 2);
        }

        if($cart_total > $delivery_cfg->freedeliveryamount)
        {
            $total_w_delivery = $cart_total;
            $deliverycosts = 0; 	//means free
        }
        else {
            $total_w_delivery = ( $cart_total + $deliverycosts );
        }


        if( $mode && $mode == 'raw' )
        {
            return array(
                'countarts' 			=> count($cart_products),
                'countitems' 			=> $cart_count,
                'total' 				=> (sprintf('%d', $cart_total)),
                'total_w_delivery' 		=> (sprintf('%d', $total_w_delivery)),
                'total_fmt' 			=> sprintf('%d', $cart_total),
                'total_fmt_w_delivery' 	=> sprintf('%d', $total_w_delivery),
                'deliverycosts' 		=> $deliverycosts,
                'deliverycosts_fmt' 	=>  sprintf('%d', $deliverycosts),
            );
        }
        else{

            $cart_html .= '<p align="center" style="font:22px \'Calibri Light Italic\'; color:rgb(113, 178, 32);; margin: 8px 0px 8px 0px;">';
            $cart_html .= '<u>'.($cart_count).' товаров</u> ';
            $cart_html .= 'на сумму: <span id="cartblock_summ"><u>'.(sprintf('%.2f', $cart_total)).'</u></span> р.</p>';
            $cart_html .= '<p align="center"><a style="font:14px \'Calibri Light Italic\', serif; color:#9a9a9a;" href="/cart/">Просмотр / Оформление заказа</a></p>';

            return $cart_html;
        }
    }
}

