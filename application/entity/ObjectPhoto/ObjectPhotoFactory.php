<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 23.08.2016
 * Time: 17:00
 */

require_once 'entity/ObjectPhoto/ObjectPhoto.php';
require_once 'models/ObjectPhotoModel.php';


class ObjectPhotoFactory
{
    private static $instance;
    private $_Id;

    protected $_model;
    protected $photo = array();

    private function __construct($Id)
    {
        $this->_model = new Application_Model_ObjectPhoto();
        $this->_parent = $Id;
        foreach ($this->_model->getObjectPhoto($this->_parent) AS $item){
            $this->pushObjectPhoto(new ObjectPhoto($item));
        }
    }

    private function pushObjectPhoto(ObjectPhoto $ObjectPhoto)
    {
        $this->photo[] = $ObjectPhoto;
    }

    public static function getInstance($Id)
    {
        self::$instance = new self($Id);
        return self::$instance;
    }

    public function get()
    {
        return $this->photo;
    }

    public function addPhoto($data)
    {
        try {
            $this->_model->addPhoto($data);
        }
        catch (Zend_Exception $e)
        {
            exit ($e);
        }
        $this->pushObjectPhoto(new ObjectPhoto($data));

        return $this;
    }

}