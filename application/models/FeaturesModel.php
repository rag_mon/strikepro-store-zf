<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr Taran
 * Date: 21.08.2016
 * Time: 17:10
 */

require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_Features extends Zend_Db_Table_Abstract
{
    public $_ID_;

    public function __construct()
    {
        parent::__construct();
        $this->_name = 'type_features';
    }

    public function getTypeFeature($type_id)
    {
        $select = $this->getAdapter()->select()
            ->from(array('tf' => 'type_features'), array('tf.features_id'))
            ->where('product_types_id = ' . $type_id);

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetchAll();

        return $result;
    }

    public function getProductFeatures($Id)
    {
        $subQuery = '(SELECT `cat_type` FROM `catalog_cats` WHERE (cat_code =' . $Id .'))';

        $select = $this->getAdapter()->select()
            ->from(array('type_features', array('tf.order', 'tf.title', 'tf.measurement', 'tf.description')))
            ->joinleft('features', 'features.id = type_features.features_id')
            ->joinleft('product_features_value',
                'product_features_value.product_code = '. $Id . ' and ' .
                'product_features_value.features_id = features.id',
                array('value', 'product_code'))
            ->where('type_features.product_types_id =  ?', new Zend_Db_Expr($subQuery));

        //echo $select; exit;

        $stmt = $this->getAdapter()->query($select);

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    public function save($data)
    {
        $where[] = $this->getAdapter()->quoteInto('product_code = ?', $data['product_code']);
        $where[] = $this->getAdapter()->quoteInto('features_id = ?', $data['features_id']);

        try {
            $this->getAdapter()->update('product_features_value', $data, $where);
        }
        catch (Zend_Db_Exception $e)
        {
            die('Something went wrong: ' . $e->getMessage());
        }
    }

    public function resetProductFeatures($type_id)
    {
        $select = $this->getAdapter()->select()
            ->from(array('cc' => 'catalog_cats'),array('cc.cat_code'))
            ->where('cc.cat_type = 1');

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetchAll();

        $type_features = $this->getTypeFeature($type_id);

        foreach($result as $code)
        {
            foreach ($type_features AS $feature) {
                $this->getAdapter()->insert('product_features_value', array('product_code' => $code['cat_code'], 'feature_id' => $feature['features_id'], 'value' => null));
            }
        }
        echo "complete"; exit;
        return;
    }
}