// -- tets -- var WebServiceUrl = "/AddressComparison.asmx";
var WebServiceUrl = "/cart/searchkladr";
var WebServiceMethodName = "?subaction=GetDataFilter";
var WebServiceFindByAddressMethodName = "?subaction=FindOPSByContextStr";
var WebServiceFindByAddressStrongMethodName = "?subaction=FindOPSByContextStrAddr";
var WebServiceFindByIndexMethodName = "?subaction=FindOPSByIndexStr";



var RegionOldValue = "-1";
var DistrictOldValue = "-1";
var SettlementOldValue = "-1";
var StreetOldValue = "-1";

var Length = 0;
var ResultContent = "";
var dataCount = 0;

var loadRegion = false;
var loadDistrict = false;
var loadSettlement = false;
var loadStreet = false;
var loadResult = false;

///--------------регионы              

$(document).ready(function () {

    $('#TextBoxFindInd').bind('keypress', function (e) {
        return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true;
    });

    $("#TextBoxFindInd").bind('keyup', function (e) { SetEnableFindButton(); $("#ResultDiv").empty(); });
    $("#TextBoxFindInd").bind('blur', function (e) { SetEnableFindButton(); });
    //$("#TextBoxFindInd").bind('onclik', function (e) { SetEnableFindButton(); });

    $("#TextBox_FindString").bind('keyup', function (e) { SetEnableFindButton(); $("#ResultDiv").empty(); });
    $("#TextBox_FindString").bind('blur', function (e) { SetEnableFindButton(); });
    //$("#TextBox_FindString").bind('onclik', function (e) { SetEnableFindButton(); });

    //wss
    //$("#RadioButtonSelMode")[0].checked = true;

    //Загружаем данные регионов              
    loadRegion = true;
    SetLoadingStatus();
    //$.getJSON(WebServiceUrl + WebServiceMethodName + '?ParentId=-1&ChildType=-1', InitRegionData);
    ChangeMode();
}
);

//очистка поля "Дом"
function ClearHouseNumber () {
    $("#HouseNumber")[0].value = "";
}

//Инициализируем данные регионов
function InitRegionData(data) {

    //var ret = $("#selectRegion")[0];

    //Очищаем список select
    $("#selectRegion").empty();
    $("#selectRegion").RemoveIncSearch();

    //Заполняем список
    $.each(data, addelementRegion);

    //Очищаем список select районы
    $("#selectDistrict").empty();
    $("#selectDistrict").RemoveIncSearch();

    //Очищаем список select нас пункты
    $("#selectSettlement").empty();
    $("#selectSettlement").RemoveIncSearch();

    //Очищаем список select улиц
    $("#selectStreet").empty();
    $("#selectStreet").RemoveIncSearch();

    RegionOldValue = $("#selectRegion")[0].value;

    
    if($('#dlvr_mode_idx').length && $('#dlvr_mode_idx').val() == 1 )
    {
        $("#selectRegion option[value=7800000000000]")[0] . selected = true;
        $("#selectRegion option[value=7800000000000]") . attr('selected', true);
        $("#selectRegion").change();
        $("#selectRegion").attr('readonly', true);
        $("#selectRegion").attr('readOnly', true);
        $("#selectRegion")[0] . readOnly = true;
        $("#selectRegion") . addClass('df');
    }
    else{
        //Включаем контектстный поиск              
        $("#selectRegion").AddIncSearch({
            maxListSize: 20,
            maxMultiMatch: 10000
        });
    }
    loadRegion = false;
    SetLoadingStatus();
    UpdateAddressStr();
}

//Добавляем позицию в список регионов              
function addelementRegion(ind, item) {
    $("#selectRegion").append('<option value="' + item.Value + '">' + item.Text + '</option>');
}

///--------------районы              

//Инициализируем данные районов              
function InitDistrictData(data) {

    //Очищаем список select районы              
    $("#selectDistrict").empty();
    $("#selectDistrict").RemoveIncSearch();

    //Заполняем список              
    $.each(data, addelementDistrict);

    //Очищаем список select нас пункты              
    //$("#selectSettlement").empty();              

    loadSettlement = true;
    SetLoadingStatus();
    //Загружаем данные нас пунктов              
    $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=' + $("#selectRegion")[0].value + '&ChildType=6', InitSettlementData);

    //Очищаем список select улиц              
    //$("#selectStreet").empty();              
    //$("#selectStreet").RemoveIncSearch();              

    loadStreet = true;
    SetLoadingStatus();
    //Загружаем данные улиц              
    $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=' + $("#selectRegion")[0].value + '&ChildType=8', InitStreetData);

    DistrictOldValue = $("#selectDistrict")[0].value;

    //Включаем контектстный поиск              
    $("#selectDistrict").AddIncSearch({
        maxListSize: 20,
        maxMultiMatch: 10000
    });

    loadDistrict = false;
    SetLoadingStatus();
    UpdateAddressStr();

}

//Добавляем позицию в список районов              
function addelementDistrict(ind, item) {
    $("#selectDistrict").append('<option value="' + item.Value + '">' + item.Text + '</option>');
}

///--------------нас пункты              

//Инициализируем данные районов              
function InitSettlementData(data) {

    //Очищаем список select нас пункты              
    $("#selectSettlement").empty();
    $("#selectSettlement").RemoveIncSearch();

    //Заполняем список               
    $.each(data, addelementSettlement);


    //Очищаем список select улиц              
    $("#selectStreet").empty();
    $("#selectStreet").RemoveIncSearch();

    SettlementOldValue = $("#selectSettlement")[0].value;

    //Включаем контектстный поиск              
    $("#selectSettlement").AddIncSearch({
        maxListSize: 20,
        maxMultiMatch: 10000
    });

    loadSettlement = false;
    SetLoadingStatus();
    UpdateAddressStr();

}

//Добавляем позицию в список районов              
function addelementSettlement(ind, item) {
    dataCount++;
    $("#selectSettlement").append('<option value="' + item.Value + '">' + item.Text + '</option>');
}


///--------------нас улицы              

//Инициализируем данные районов              
function InitStreetData(data) {

    //Очищаем список select улиц              
    $("#selectStreet").empty();
    $("#selectStreet").RemoveIncSearch();

    //Заполняем список              
    $.each(data, addelementStreet);

    //Включаем контектстный поиск              
    $("#selectStreet").AddIncSearch({
        maxListSize: 20,
        maxMultiMatch: 10000
    });

    loadStreet = false;
    SetLoadingStatus();
    UpdateAddressStr();
}

//Добавляем позицию в список улиц              
function addelementStreet(ind, item) {
    $("#selectStreet").append('<option value="' + item.Value + '">' + item.Text + '</option>');
}

//--------------- Логика              


//выбрали регион              
function SelectRegion(regObj, isReload) {

    var val = regObj.value;
    if (RegionOldValue == val && !isReload) {
        return;
    }
    else {
        RegionOldValue = val;
    }

    //обновляем строку адреса              
    UpdateAddressStr();
    //очищаем строку с номером дома
    ClearHouseNumber();
    loadDistrict = true;
    SetLoadingStatus();
    //Загружаем данные районов              
    $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=' + val + '&ChildType=5', InitDistrictData);

}

//выбрали район              
function SelectDistrict(disObj) {
    var val = disObj.value;
    if (DistrictOldValue == val) {
        return;
    }
    else {
        DistrictOldValue = val;
    }
    if (val == "-1")
    {
        SelectRegion({value: RegionOldValue}, true);
        return;
    }
    //обновляем строку адреса              
    UpdateAddressStr();
    //очищаем строку с номером дома
    ClearHouseNumber();

    loadSettlement = true;
    SetLoadingStatus();
    //Загружаем данные нас пунктов              
    $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=' + val + '&ChildType=6', InitSettlementData);

}

//выбрали нас пункт              
function SelectSettlement(setObj) {


    if (SettlementOldValue == setObj.value) {
        return;
    }
    else {
        SettlementOldValue = setObj.value;
    }

    //обновляем строку адреса              
    UpdateAddressStr();
    //очищаем строку с номером дома
    ClearHouseNumber();

    loadStreet = true;
    SetLoadingStatus();
    //Загружаем данные улиц              
    $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=' + setObj.value + '&ChildType=8', InitStreetData);

}

//выбрали улицу              
function SelectStreet(strObj) {

    if (StreetOldValue == strObj.value) {
        return;
    }
    else {
        StreetOldValue = strObj.value;
    }

    //очищаем строку с номером дома
    ClearHouseNumber();
    //обновляем строку адреса              
    UpdateAddressStr();

}


//Изминили номер дома              
function HouseNumberChange() {
    //обновляем строку адреса
    
    
    var strcode = $('#selectStreet').val();
    var housenum = $('#HouseNumber').val();
    
    if(parseInt(strcode)){
        ShowLoader();
        
            ajax_rep=$.ajax({
                type : "GET",
                url : ($('head').data('site_url'))+'/cart/searchkladr',
                contentType : "text/html",
                dataType : 'html',
                global: false,
                async:false,
                data : {
                    'rnd':randy_by_len(6),
                    'subaction':'getIdxByStrHouseNum',
                    'strcode':strcode,
                    'housenum':housenum
                    
                }
            }).responseText;
            
            if(ajax_rep){
                ajax_rep = $.parseJSON(ajax_rep);
            }
            
            if(ajax_rep && typeof(ajax_rep)=="object"){
                var idx = ajax_rep.houseindex;
                if(idx && parseInt(idx)>0){
                    //upd 24.08
                    //if($('#IndexNumber').val()==''){
                        $('#IndexNumber').val(idx);
                    //}
                }
                else{
                    $.jGrowl('Индекс автоматически не найден', {
                             speed:  'slow'
                    });
                }
            }
            else{
                $.jGrowl('Ошибка получения данных с сервера', {
                         speed:  'slow'
                });
            }
        
        HideLoader();
    }
     
    UpdateAddressStr();
}

//Изминили index
function IndexNumberChange() {
    //обновляем строку адреса              
    UpdateAddressStr();
}

//Изминили кв
function ApartmentNumberChange() {
    //обновляем строку адреса              
    UpdateAddressStr();
}

//Изминили дробь
function HouseSubNumberChange() {
    //обновляем строку адреса              
    UpdateAddressStr();
}

//Возвращает текст эл управления Selct               
function GetSelectedTextFromSelect(selObj) {

    if (selObj.selectedIndex == -1) {
        return "";
    }

    return selObj.options[selObj.selectedIndex].text;
}


//обновляет строку адреса              
function UpdateAddressStr() {


    //очищаем строку адреса              
    $("#ResultAddress").empty();

    //Добавляем составляющие адреса              

    //дом              
    if ($("#IndexNumber")[0].value != "") {
        $("#ResultAddress").append($("#IndexNumber")[0].value+' ');
    }

    //регион              
    if (GetSelectedTextFromSelect($("#selectRegion")[0]) != "") {
        $("#ResultAddress").append(GetSelectedTextFromSelect($("#selectRegion")[0]) + " ");
    }

    //район              
    if (GetSelectedTextFromSelect($("#selectDistrict")[0]) != "") {
        $("#ResultAddress").append(GetSelectedTextFromSelect($("#selectDistrict")[0]) + " ");
    }

    //нас пункт              
    var nas_punkt_txt = ''
    if (GetSelectedTextFromSelect($("#selectSettlement")[0]) != "") {
        nas_punkt_txt = GetSelectedTextFromSelect($("#selectSettlement")[0]);
        nas_punkt_txt = nas_punkt_txt.replace(/^\d+\s+/, '');
        $("#ResultAddress").append(nas_punkt_txt + " ");
    }

    //улица              
    if (GetSelectedTextFromSelect($("#selectStreet")[0]) != "") {
        $("#ResultAddress").append(GetSelectedTextFromSelect($("#selectStreet")[0]) + " ");
    }

    //дом              
    if ($("#HouseNumber")[0].value != "") {
        $("#ResultAddress").append("дом " + $("#HouseNumber")[0].value);
        
        //корп
        if ($("#HouseSubNumber")[0].value != "") {
            if ($("#HouseSubNumber")[0].value.match(/^\d+/)) {
                $("#ResultAddress").append('/');
            }
            $("#ResultAddress").append($("#HouseSubNumber")[0].value + " ");
        }
        else{
            $("#ResultAddress").append(" ");
        }
        
    }

    //квартира
    if ($("#ApartmentNumber")[0].value != "") {
        $("#ResultAddress").append("кв. " + $("#ApartmentNumber")[0].value);
    }
    
    $("#ResultAddressHidden").val($("#ResultAddress").text());

    //Устанавливаем статус кнопки найти              
    SetEnableFindButton();

    //очищаем результат              
    $("#ResultDiv").empty();

}


//-----------режим              


//получает номер режима              
function GetFindMode() {
    
    //wss
    return 0;

    if ($("#RadioButtonSelMode")[0].checked) {
        return 0;
    }

    if ($("#RadioButtonAddrStrMode")[0].checked) {
        return 1;
    }

    return 3;

}


//----------- поиск              


//устанавливает кнопку поиск              
function SetEnableFindButton() {

    var resultEnable = false;


    switch (GetFindMode()) {
        case 0:
            {
                if (jQuery.trim($("#ResultAddress")[0].innerHTML) != '') {
                    resultEnable = true;
                }
                break;
            }
        case 1:
            {
                if (jQuery.trim($("#TextBox_FindString")[0].value) != '') {
                    resultEnable = true;
                }
                break;
            }
        default:
            {
                if (jQuery.trim($("#TextBoxFindInd")[0].value) != '' && jQuery.trim($("#TextBoxFindInd")[0].value).length == 6) {
                    resultEnable = true;
                }
                break;
            }
    }

    if (resultEnable) {
        $("#ButtonFindOPS")[0].disabled = false;
    }
    else {
        $("#ButtonFindOPS")[0].disabled = true;
    }



}

//Меняем режим отображения              
function ChangeMode() {


//wss
/*
                //Включаем selects              
                $("#selectRegion")[0].disabled = false;
                $("#selectRegion")[0].className = 'enable noenter';


                $("#selectDistrict")[0].disabled = false;
                $("#selectDistrict")[0].className = 'enable noenter';


                $("#selectSettlement")[0].disabled = false;
                $("#selectSettlement")[0].className = 'enable noenter';


                $("#selectStreet")[0].disabled = false;
                $("#selectStreet")[0].className = 'enable noenter';

                $("#HouseNumber")[0].disabled = false;
                $("#HouseNumber")[0].className = 'enable noenter';
*/
                //Загружаем данные регионов              
                $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=-1&ChildType=-1', InitRegionData);

return;


    //очищаем результат              
    $("#ResultDiv").empty();

    switch (GetFindMode()) {
        case 0:
            {

/*//WSS

                //очищаем поле строки поиска по адресу
                $("#TextBox_FindString")[0].value = '';

                //очищаем поле строки поиска по адресу
                $("#TextBoxFindInd")[0].value = '';

                //отключаем индексы              
                $("#TextBoxFindInd")[0].disabled = true;
                $("#TextBoxFindInd")[0].className = 'disable';

                //отключаем Строку адреса              
                $("#TextBox_FindString")[0].disabled = true;
                $("#TextBox_FindString")[0].className = 'disable';

*/

                //Включаем selects              
                $("#selectRegion")[0].disabled = false;
                $("#selectRegion")[0].className = 'enable noenter';


                $("#selectDistrict")[0].disabled = false;
                $("#selectDistrict")[0].className = 'enable noenter';


                $("#selectSettlement")[0].disabled = false;
                $("#selectSettlement")[0].className = 'enable noenter';


                $("#selectStreet")[0].disabled = false;
                $("#selectStreet")[0].className = 'enable noenter';

                $("#HouseNumber")[0].disabled = false;
                $("#HouseNumber")[0].className = 'enable noenter';

                //Загружаем данные регионов              
                $.getJSON(WebServiceUrl + WebServiceMethodName + '&ParentId=-1&ChildType=-1', InitRegionData);

                break;
            }
        case 1:
            {


                //очищаем поле строки поиска по адресу
                $("#TextBoxFindInd")[0].value = '';

                //отключаем индексы              
                $("#TextBoxFindInd")[0].disabled = true;
                $("#TextBoxFindInd")[0].className = 'disable';


                //включаем Строку адреса              
                $("#TextBox_FindString")[0].disabled = false;
                $("#TextBox_FindString")[0].className = 'enable';

                //отключаем selects              
                $("#selectRegion").RemoveIncSearch();
                $("#selectRegion")[0].disabled = true;
                $("#selectRegion")[0].className = 'disable';
                $("#selectRegion")[0].value = '-1';

                $("#selectDistrict").RemoveIncSearch();
                $("#selectDistrict")[0].disabled = true;
                $("#selectDistrict")[0].className = 'disable';
                $("#selectDistrict")[0].value = '-1';

                $("#selectSettlement").RemoveIncSearch();
                $("#selectSettlement")[0].disabled = true;
                $("#selectSettlement")[0].className = 'disable';
                $("#selectSettlement")[0].value = '-1';

                $("#selectStreet").RemoveIncSearch();
                $("#selectStreet")[0].disabled = true;
                $("#selectStreet")[0].className = 'disable';
                $("#selectStreet")[0].value = '-1';

                $("#HouseNumber")[0].disabled = true;
                $("#HouseNumber")[0].className = 'disable';
                $("#HouseNumber")[0].value = '';

                UpdateAddressStr();

                break;
            }
        default:
            {

                //очищаем поле строки поиска по адресу
                $("#TextBox_FindString")[0].value = '';

                //включаем индексы              
                $("#TextBoxFindInd")[0].disabled = false;
                $("#TextBoxFindInd")[0].className = 'enable';

                //отключаем Строку адреса              
                $("#TextBox_FindString")[0].disabled = true;
                $("#TextBox_FindString")[0].className = 'disable';

                //отключаем selects              
                $("#selectRegion").RemoveIncSearch();
                $("#selectRegion")[0].disabled = true;
                $("#selectRegion")[0].className = 'disable';
                $("#selectRegion")[0].value = '-1';


                $("#selectDistrict").RemoveIncSearch();
                $("#selectDistrict")[0].disabled = true;
                $("#selectDistrict")[0].className = 'disable';
                $("#selectDistrict")[0].value = '-1';


                $("#selectSettlement").RemoveIncSearch();
                $("#selectSettlement")[0].disabled = true;
                $("#selectSettlement")[0].className = 'disable';
                $("#selectSettlement")[0].value = '-1';


                $("#selectStreet").RemoveIncSearch();
                $("#selectStreet")[0].disabled = true;
                $("#selectStreet")[0].className = 'disable';
                $("#selectStreet")[0].value = '-1';

                $("#HouseNumber")[0].disabled = true;
                $("#HouseNumber")[0].className = 'disable';
                $("#HouseNumber")[0].value = '';

                UpdateAddressStr();


                break;
            }
    }

}


function Find() {

    switch (GetFindMode()) {
        case 0:
            {
                FindOPSByAddressStrongStr($("#ResultAddress")[0].innerHTML);
                break;
            }
        case 1:
            {
                FindOPSByAddressStr($("#TextBox_FindString")[0].value);
                break;
            }
        default:
            {
                FindOPSByIndexStr($("#TextBoxFindInd")[0].value);
                break;
            }
    }

}


//находим адрес по строке поиска              
function FindOPSByAddressStrongStr(AddressStr) {

    loadResult = true;
    SetLoadingStatus();
    //Загружаем данные улиц              
    $.getJSON(WebServiceUrl + WebServiceFindByAddressStrongMethodName + '&ContextStr=' + escape(AddressStr), ShowFindAddressData);

}

//находим адрес по строке поиска              
function FindOPSByAddressStr(AddressStr) {

    loadResult = true;
    SetLoadingStatus();
    //Загружаем данные улиц              
    $.getJSON(WebServiceUrl + WebServiceFindByAddressMethodName + '&ContextStr=' + escape(AddressStr), ShowFindAddressData);

}

//находим адрес по строке поиска              
function FindOPSByIndexStr(IndexStr) {

    loadResult = true;
    SetLoadingStatus();
    //Загружаем данные улиц              
    $.getJSON(WebServiceUrl + WebServiceFindByIndexMethodName + '&IndexStr=' + escape(IndexStr), ShowFindAddressData);

}

function ShowFindAddressData(FindData) {

    var rfg = FindData;

    Length = 0;

    //очищаем результат              
    $("#ResultDiv").empty();

    //Записываем заголовок таблицы              
    ResultContent = '<a name="resultancor"></a><table class="resultTable" cellspacing="0" cellpadding="5" border="0" style="color:Black;border-collapse:collapse;"><tr class="tableHeader" style="color:#003399;background-color:#99CCFF;font-weight:bold;"><th scope="col">№</th><th scope="col">Индекс</th><th scope="col">Название объекта</th><th scope="col">Адрес</th><th scope="col">Телефон</th></tr>';

    //Выводим результат              
    $.each(FindData, addResultTableElement);

    //Записываем завершение таблицы              
    ResultContent += '</table>';


    if (Length != 0) {

        if (FindData[1].Status == 2 || FindData[1].Status == 3) {
            ResultContent += '<span class="message"><br />Запрашиваемый Вами дом не найден, но есть схожие записи.<br /></span>';
        }

        $("#ResultDiv").append(ResultContent);

    }
    else {

        if (GetFindMode() == 3) {
            $("#ResultDiv").append('<a name="resultancor"></a><span class="message"><br/>Объекты почтовой связи с данным индексом не существуют! <br />Проверьте правильность ввода индекса либо воспользуйтесь поиском ОПС по зоне обслуживания.<br/></span>');
        }
        else {
            $("#ResultDiv").append('<a name="resultancor"></a><span class="message"><br />По Вашему запросу ничего не найдено.<br /></span>');
        }
    }

    loadResult = false;
    SetLoadingStatus();

    jumpToAnchor("#resultancor");

}

function addResultTableElement(ind, item) {

    if (ind == 0) {
        return;
    }

    Length = ind;

    //Записываем Найденный ОПС              

    if (ind % 2 == 0) {
        ResultContent += '<tr style="background-color:#EEEEEE;">';
    }
    else {
        ResultContent += '<tr style="background-color:#FFFFFF;">';
    }

    ResultContent += '<td>' + item.Number + '</td>';

    ResultContent += '<td align="center" style="width:200px;"><a href="javascript:viewIndexData(' + item.Index + ')" title="информация об ОПС" >' + item.Index + '</a></td>';

    ResultContent += '<td>' + item.Name + '</td>';

    ResultContent += '<td>' + item.Address + '</td>';

    ResultContent += '<td>' + item.Phone + '</td>';

    ResultContent += '</tr>';

}


var newWin;

function viewIndexData(index) {

    if (newWin == null || newWin.closed) {
        newWin = OpenNewWinNoResize('http://www.russianpost.ru/PostOfficeFindInterface/FindOPSByPostOfficeID.aspx?index=' + index, 880, 750);
    }
    newWin.focus();
}


function OpenNewWinNoResize(Surl, SW, SH) {
    var newwin = window.open('', 'Информация', 'height=' + SH + ',width=' + SW + ',Toolbar=0,Directories=0,Status=0,Menubar=0,Scrollbars=1,Resizable=1');
    newwin.document.write('<body></body>');

    //                var windowwidth, windowheight;   
    //                if (window.innerWidth) {   
    //                    windowwidth = newwin.outerWidth;   
    //                    windowheight = newwin.outerHeight;   
    //                } else {   
    //                    windowwidth = newwin.document.body.clientWidth;   
    //                    windowheight = newwin.document.body.clientHeight;   
    //                }   
    //                var screenwidth = screen.availWidth;   
    //                var screenheight = screen.availHeight;   
    //                newwin.moveTo(   
    //              Math.round((screenwidth - windowwidth) / 2),   
    //              Math.round((screenheight - windowheight) / 2));   
    newwin.location.href = Surl;
    return newwin;
}


//осуществляет переход к якорю
function jumpToAnchor(myAnchor) {
    window.location = String(window.location).replace(/\#.*$/, "") + myAnchor;
}


//wss
//отображение loading-колесика
function ShowLoader() {
    
    //wss
    $('#wss_po_loader').removeClass('dn');
    return;
    
    var l_loader = document.getElementById("loader");
    if (l_loader) {
        l_loader.style.display = "block";
    }
}
//скрытие loading-колесика
function HideLoader() {
    
    //wss
    $('#wss_po_loader').addClass('dn');
    return;
    
    var l_loader = document.getElementById("loader");
    if (l_loader) {
        l_loader.style.display = "none";
    }
}


//отражает\скрывает иконку загрузки ресурса
function SetLoadingStatus() {
    if (loadRegion || loadDistrict || loadSettlement || loadStreet || loadResult) {
        //отображаем изображение  загрузки
        ShowLoader();
    }
    else {
        //скрываем изображение загрузки
        HideLoader();
    }
}


