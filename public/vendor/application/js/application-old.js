/**
 * Created by Aleksandr Taran on 30.08.2016.
 */
$(function() {

    $("#only_insale").click(function () {
        if($("#only_insale").is(':checked')) {
            $("img[data-insale='0']").fadeTo( "slow", 0.1 );
        }
        else {
            $("img[data-insale='0']").fadeTo( "slow", 1.0 );
        }
    });
});