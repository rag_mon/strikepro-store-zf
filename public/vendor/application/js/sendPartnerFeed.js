/**
 * Created by Aleksandr Taran on 05.09.2016.
 */
jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
}, "<br />Пожалуйста, введите номер телефона в формате +71234567890");

jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[а-я,a-z]+$/i.test(value);
}, "Только буквы, пожалуйста");

$().ready(function()
{
    $("#partner_form").validate({
        rules: {
            firstname: {
                required: true,
                minlength: 1,
                lettersonly: true,

            },
            lastname: {
                required: true,
                minlength: 1,
                lettersonly: true,
            },
            phone: {
                required: true,
                minlength: 7,
                phoneno:true
            },
            email: {
                required: true,
                email: true,
            },
            legalname: {
                required: true,
                minlength: 5,
            },
            address: {
                required: true,
                minlength: 3,
            },
            message: {
                required: true,
                minlength: 10,
            },
            business_type : {
                required: true,
                minlength: 5,
            }
        },
        messages: {
            firstname: {
                required: "Пожалуйста введите ваше имя.",
                minlength: "Минимально один символ."
            },
            lastname: {
                required: "Пожалуйста введите вашу фамилию.",
                minlength: "Минимально один символ."
            },
            phone: {
                required: "Пожалуйста введите номер телефона.",
                minlength: "Минимально семь цифр.",
            },
            email: {
                required: "Пожалуйста, введите ваш адрес электронной почты.",
                email: "Пожалуйста, введите правильный адрес электронной почты в формате example@example.com",
            },
            legalname: {
                required: "Название юридического лица обязательно для заполнения.",
                minlength: "Минимально пять символов.",
            },
            address: {
                required: "Название населенного пункта обязательно для заполнения.",
                minlength: "Название не может быть меньше трех символов.",
            },
            business_type : {
                required: "Пожалуйста, заполните это поле.",
                minlength: "Поле должно содержать хотя бы пять символов.",
            },
            message: {
                required: "Пожалуйста, опишите цель вашего обращения.",
                minlength: "Минимально 10 символов."
            }
        }
    });
});

function sendPartnerFeed()
{
    var form = $("#partner_form");

    if(form.valid()) {
        $.ajax({
            type: 'POST',
            url: ($('head').data('site_url')) + '/feedback/partner',
            data: {
                'firstname': $('#firstname').val(),
                'lastname': $('#lastname').val(),
                'phone': $('#phone').val(),
                'email': $('#email').val(),
                'legalname': $('#legalname').val(),
                'address': $('#address').val(),
                'business_type': $('#business_type').val(),
                'message': $('#message').val()
            },
            success: function (response) {
                if (response.status == 'ok') {
                    $('#request_number').text(response.request_number);
                    $('#partner_form').fadeOut(0);
                    $('#success_add_request').fadeIn(3000);
                }
                return;
            },
            fail: function() {
                alert("Ошибка! Повторите попытку позже.")
            }
        });
    }
    else {
        return;
    }
}