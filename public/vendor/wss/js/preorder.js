/**
 * Created by Aleksandr Taran on 04.09.2016.
 */
$(document).ready(function(){

    $('#preorderform input.noenter').keypress(function(e) {
        alert(true);
        if (e.keyCode == '13'){
            e.preventDefault();
            $(e.target).change();
            return false;
        }
        return true;
    });

    $('#preorderform select.noenter').keypress(function(e) {
        if (e.keyCode == '13'){
            e.preventDefault();
            $(e.target).change();
            return false;
        }
        return true;
    });
});