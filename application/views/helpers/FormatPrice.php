<?php
//@author WSS http://websitespb.ru/, ffl.public@gmail.com
    class STPR_View_Helper_FormatPrice extends Zend_View_Helper_Abstract
    {
      
        protected $_last_error = false;
        
        public function FormatPrice($price){
            return sprintf('%.2f', $price);
        }
        
    }
