<?php
require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_PostModel extends Zend_Db_Table_Abstract
{

    protected $_name = 'posts';
    protected $limit;
    protected $order;
    protected $where;

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->where($this->getAdapter()->quoteInto('id= ?', $id));

        $stmt = $this->getAdapter()->query($select);
        $result = $stmt->fetch();

        return $result;
    }

    public function first()
    {
        $select = $this->getAdapter()->select()->from($this->_name);

        if (is_array($this->where) && count($this->where) > 0) {
            foreach ($this->where AS $where) {

                if (count($where) == 2) {
                    $select->where($this->getAdapter()->quoteInto($where[0] . ' =?', $where[1]));
                }
                if (count($where) == 3) {
                    $select->where($this->getAdapter()->quoteInto($where[0] . ' ' . $where[1] . ' ' . $where[2]));
                }
            }
        }

        if (is_array($this->order)) {
            $select->order($this->order[0] . ' ' . $this->order[1]);
        }

        if ($this->limit > 0) {
            $select->limit($this->limit);
        }

        return $this->getAdapter()->query($select)->fetch();
    }

    public function limit($count)
    {
        $this->limit = $count;
    }

    public function order($field, $sort)
    {
        $this->order = [$field, $sort];
    }

    public function where($where)
    {
        $this->where[] = $where;
    }

    public function destroy($Id)
    {
        return $this->getAdapter()->delete($this->_name, array('Id = ?' => $Id));
    }

    public function all()
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name);

        if (is_array($this->where) && count($this->where) > 0) {
            foreach ($this->where AS $where) {

                if (count($where) == 2) {

                    $select->where($this->getAdapter()->quoteInto($where[0] . ' =?', $where[1]));
                }
                if (count($where) == 3) {
                    $select->where($where[0] . ' ' . $where[1] . " '" . $where[2]. "'");
                }
            }
        }

        if (is_array($this->order)) {
            $select->order($this->order[0] . ' ' . $this->order[1]);
        }

        if ($this->limit > 0) {
            $select->limit($this->limit);
        }
        $stmt = $this->getAdapter()->query($select);

        // Получение данных
        $result = $stmt->fetchAll();

        if (count($result) > 0) {

            foreach ($result as $row) {
                $entities[] = new Application_Entity_Post_Post($row);
            }
            return $entities;
        }
        else {
            return null;
        }
    }

}